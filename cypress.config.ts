import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    }
  },

  chromeWebSecurity: false, // Disable same-origin policy, may impact security
  env: {
    CYPRESS_INTERNAL_SKIP_ONBOARDING: true // Skip onboarding messages
  }
});
