const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");

module.exports = {
  webpack: {
    configure: (config) => {
      const wasmExtensionRegExp = /\.wasm$/;
      config.resolve.extensions.push(".wasm");
      config.experiments = {
        syncWebAssembly: true,
        asyncWebAssembly: true,
      };

      config.module.rules.forEach((rule) => {
        (rule.oneOf || []).forEach((oneOf) => {
          if (oneOf.type === "asset/resource") {
            oneOf.exclude.push(wasmExtensionRegExp);
          }
        });
      });

      const scopePluginIndex = config.resolve.plugins.findIndex(
        ({ constructor }) =>
          constructor && constructor.name === "ModuleScopePlugin"
      );

      config.resolve.plugins.splice(scopePluginIndex, 1);

      return config;
    },
    plugins: [new NodePolyfillPlugin()],
  },
};
