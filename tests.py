import unittest
from unittest.mock import patch
import threading
import time
import os

from server import start_server

class TestServer(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.ip_address = "127.0.0.1"
        cls.port = 8080
        cls.hostname = "localhost"

    def setUp(self):
        self.server_thread = threading.Thread(target=start_server, args=(self.ip_address, self.port))
        self.server_thread.start()

    def tearDown(self):
        self.server_thread.join()

    def test_start_server(self):
        # Test that the server is running and returns a 200 response
        import requests
        response = requests.get(f"http://{self.ip_address}:{self.port}")
        self.assertEqual(response.status_code, 200)

    @patch('webbrowser.open_new')
    def test_web_browser_opened(self, mock_webbrowser):
        # Test that the web browser is opened to the correct URL
        start_server(self.ip_address, self.port)
        expected_url = f"http://{self.ip_address}:{self.port}"
        mock_webbrowser.assert_called_with(expected_url)

    def test_invalid_port_number(self):
        # Test that an invalid port number raises a ValueError
        with self.assertRaises(ValueError):
            start_server(self.ip_address, "not_a_port")

    def test_missing_port_number(self):
        # Test that running the script without a port number prints an error message
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            with self.assertRaises(SystemExit):
                os.system("python server.py")
                self.assertIn("Please provide the port number", mock_stdout.getvalue())

if __name__ == '__main__':
    unittest.main()
