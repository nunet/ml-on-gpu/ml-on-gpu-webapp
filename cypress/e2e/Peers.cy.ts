describe('Tokenomics', () => {
  beforeEach(() => {
    cy.visit('http://localhost:9991');

    cy.get(':nth-child(1) > .rs-form-control > .rs-input').type(
      'https://example.com'
    );

    cy.contains('Next').as('nextButton');
    cy.get('@nextButton').click();
    cy.get('.rs-form-control > .rs-input').clear().type('50');
    cy.get('.gr-btn').click();
  });

  it('goes back to page 2', () => {
    cy.get('.rs-panel-title > .rs-btn-icon').click();
    cy.contains('Tokenomics').should('be.visible');
  });

  it('hides table when choosing recommended peer', () => {
    cy.get('.rs-checkbox-wrapper').click();
    cy.get('.rs-table').should('not.exist');
  });

  it('solves ReCAPTCHA', () => {
    cy.get('.rs-checkbox-wrapper').click();
    cy.solveGoogleReCAPTCHA();
    cy.wait(5000);
    cy.get('.gr-btn').should('not.be.disabled');
  });
});
