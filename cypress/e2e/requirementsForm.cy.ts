describe('GPU ML/Computing Requirements Form', () => {
  beforeEach(() => {
    cy.visit('http://localhost:9991');
  });

  it('Loads with all expected fields and title', () => {
    cy.contains('Enter your GPU ML/Computing Requirements').should(
      'be.visible'
    );
    cy.get(':nth-child(1) > .rs-form-control > .rs-input').should('exist');
    cy.get('[d="M2 7h12a1 1 0 010 2H2a1 1 0 010-2z"]').should('exist');
    cy.get(':nth-child(6) > .rs-form-control-label').should('exist');
  });

  it('Allows only one requirements to be selected at a time', () => {
    cy.get('[name="machine_type"]').contains('GPU').click();
    cy.get('[name="machine_type"]').contains('CPU').should('not.be.checked');
  });

  it('Allows only one Usage to be selected at a time', () => {
    cy.get('[name="complexity"]').contains('Moderate').click();
    cy.get('[name="complexity"]').contains('Low').should('not.be.checked');
    cy.get('[name="complexity"]').contains('High').should('not.be.checked');
  });

  it('Allows only one framework to be selected at a time', () => {
    cy.get('[label="Which framework does it use?"]')
      .contains('PyTorch')
      .click();
    cy.get('[label="Which framework does it use?"]')
      .contains('TensorFlow')
      .should('not.be.checked');
    cy.get('[label="Which framework does it use?"]')
      .contains('PyOpenCL')
      .should('not.be.checked');
  });

  it('Adds a new input for frameworks or dependencies when clicking the plus button', () => {
    cy.get(
      '[style="position: relative; background: rgb(245, 245, 245); padding: 10px; border-radius: 5px;"] > .rs-form-control-label'
    ).click();
    cy.get('.small_placeholder').should('be.visible');
  });

  it('Deletes frameworks or dependencies when clicking the trash button', () => {
    cy.get(
      '[style="position: relative; background: rgb(245, 245, 245); padding: 10px; border-radius: 5px;"] > .rs-form-control-label'
    ).click();

    cy.get('[style="display: flex;"] > .rs-btn-icon').click();

    cy.get('.small_placeholder').should('not.exist');
  });

  it('Shows error when clicking "Next" without typing the URL', () => {
    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();
    cy.get('.alert_box').should('be.visible');
  });

  it('should display an error for invalid input', () => {
    const invalidInput = 'invalid-value';
    cy.get(':nth-child(1) > .rs-form-control > .rs-input').type(invalidInput);
    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();
    cy.get('.alert_box').should('be.visible');
  });

  it('Switch to page 2 when there is a link written', () => {
    cy.get(':nth-child(1) > .rs-form-control > .rs-input').type(
      'https://example.com'
    );

    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();

    cy.contains('Tokenomics').should('exist');
  });

  it('does not switch when time equals 0', () => {
    cy.get(':nth-child(1) > .rs-form-control > .rs-input')
      .clear()
      .type('https://example.com');

    cy.get(':nth-child(6) > .rs-form-control > .rs-input').clear().type('0');

    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();

    cy.contains('Tokenomics').should('not.exist');
  });

  it('switches pages when time greater than 0', () => {
    cy.get(':nth-child(1) > .rs-form-control > .rs-input').type(
      'https://example.com'
    );

    cy.get(':nth-child(6) > .rs-form-control > .rs-input').type('3');

    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();

    cy.contains('Tokenomics').should('exist');
  });

  it('should have time set default to 1', () => {
    cy.get(':nth-child(6) > .rs-form-control > .rs-input')
      .invoke('val')
      .should('eq', '1');
  });
});
