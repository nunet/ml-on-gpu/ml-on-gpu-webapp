describe('Tokenomics', () => {
  beforeEach(() => {
    cy.visit('http://localhost:9991');

    cy.get(':nth-child(1) > .rs-form-control > .rs-input').type(
      'https://example.com'
    );

    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();
  });

  it('Checks default value of ntx is zero', () => {
    cy.get('.rs-form-control > .rs-input').invoke('val').should('eq', '0');
  });

  it('"Next" should be disabled when NTX equals zero', () => {
    cy.get('.gr-btn').should('be.disabled');
  });

  it('enables the button when we write NTX value', () => {
    cy.get('.rs-form-control > .rs-input').clear().type('50');
    cy.get('.gr-btn').should('not.be.disabled');
  });

  it('Switches to page 3', () => {
    cy.get('.rs-form-control > .rs-input').clear().type('50');
    cy.get('.gr-btn').click();
    cy.contains('Select a peer').should('be.visible');
  });

  it('goes back to page 1', () => {
    cy.get('.rs-panel-title > .rs-btn-icon').click();
    cy.contains('Enter your GPU ML/Computing Requirements').should(
      'be.visible'
    );
  });
});
