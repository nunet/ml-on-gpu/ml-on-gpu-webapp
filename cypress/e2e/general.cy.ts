describe('App', () => {
  beforeEach(() => {
    cy.visit('http://localhost:9991');
  });

  it('should be connected to DMS', () => {
    cy.contains('Connected').should('be.visible');
  });

  it('sends notifications on error', () => {
    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();
    cy.get('.rs-dropdown > .rs-navbar-item').should('contain', '1');
  });
  
    it('does not show notifications list when empty', () => {
    cy.get('.rs-dropdown > .rs-navbar-item').click();

    cy.get('.rs-dropdown > .rs-navbar-item').should('not.contain', '1');
    cy.get('.rs-dropdown > .rs-navbar-item').should('not.contain', 'Clear Notifications');
  });

  it('clears notifications', () => {
    cy.contains('Next').as('nextButton');

    cy.get('@nextButton').click();
    cy.get('.rs-dropdown > .rs-navbar-item').click();
    cy.contains('Clear Notifications').click();

    cy.get('.rs-dropdown > .rs-navbar-item').should('not.contain', '1');
  });
});
