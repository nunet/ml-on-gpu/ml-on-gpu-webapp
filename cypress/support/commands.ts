/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

Cypress.on('window:before:load', (win) => {
  win.appEnvironment = {
    myCustomValue: 'Hello, Cypress!'
  };
});

Cypress.Commands.add('solveGoogleReCAPTCHA', () => {
  cy.wait(5000);
  cy.get('[style="width: 304px; height: 78px;"] > div > iframe').then(
    ($iframe) => {
      const $body = $iframe.contents().find('body');
      $body.find('.recaptcha-checkbox').click();
    }
  );
});

declare namespace Cypress {
  interface Chainable {
    solveGoogleReCAPTCHA(
      value?: string /* eslint-disable-line */
    ): Chainable<JQuery<HTMLElement>>;
  }
}
