title Compute Request Web App

participant Web App Form
participant Wallet
participant DMS

Web App Form -> DMS: Send machine data
alt success
DMS -> Web App Form: Wallet metadata and address
else failure
DMS -> Web App Form: failed
end

Web App Form -> Wallet: Complete transaction
alt success
Web App Form -> DMS: <<send status>> success
note right of Web App Form: Wait for request to finish
else failure
Web App Form -> DMS: <<send status>> failure
end

DMS -> Web App Form: Deployment gist link
