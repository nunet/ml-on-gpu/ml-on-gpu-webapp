<!-- New changes go on top. But below these comments. -->

<!-- We can track changes from Git logs, so why we needs this file?

Guiding Principles
- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

-->
## [0.2.5](#22)
### Added
- added e2e tests using cypress

## [0.2.3](#9)
### Added
- added the logic selecting a peer manually or choosing recommended

## [0.2.2](#56)
### Added
- added the logic for transaction sync with dms

## [0.2.1](#48)
### Added
- added the logic for resuming a job
- updated the link for  PyOpenCL

## [0.2.0](#46)
### Added
- transactions table 
- transactions operations
- filtering
## [0.1.10](#45)
### Added
- handler to catch the funds errors from the wallet and display them
## [0.1.9](#44)
### Changed
- peer list changes
- text fix
- bg removed
## [0.1.8](#42)
### Changed
- Include tx hash in send-status for depreq

# [0.1.7](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-webapp/-/tree/v0.1.7)

### Fixed
- Max NTX positive values only (!55, !56)
- Upgrade dependencies and build script fix (!58)
