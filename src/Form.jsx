import React, { useState, useContext } from 'react';
import {
  Container,
  Header,
  Content,
  Form,
  Badge,
  ButtonToolbar,
  Button,
  Navbar,
  Panel,
  FlexboxGrid,
  Radio,
  RadioGroup,
  Whisper,
  Tooltip,
  IconButton,
  Loader,
  Nav,
  Input,
  Checkbox,
  Divider
} from 'rsuite';
import PlusIcon from '@rsuite/icons/Plus';
import TrashIcon from '@rsuite/icons/Trash';
import PageNext from '@rsuite/icons/PageNext';
import ArowBack from '@rsuite/icons/ArowBack';
import NewWindow from 'react-new-window';
import CheckOutline from '@rsuite/icons/CheckOutline';
import CloseOutline from '@rsuite/icons/CloseOutline';
import InfoRoundIcon from '@rsuite/icons/InfoRound';
import NoticeIcon from '@rsuite/icons/Notice';
import HelpOutlineIcon from '@rsuite/icons/HelpOutline';
import GlobalIcon from '@rsuite/icons/Global';
import { ToastContainer, toast } from 'react-toastify';
import { w3cwebsocket as W3CWebSocket } from 'websocket';
import moment from 'moment';
import { JobSubmittedModal } from './components/JobSubmittedModal';
import { FailedPopup } from './components/FailedPopup';
import { PeersPopupPage } from './components/PeersPopup';
import Logo from './logo.png';

import Table from 'components/Table';
import { _getAllUserUtxos, _getDatum } from './tokens/utils';
import { UnLockAssetsInContract } from './tokens/unlockContract';

import { BlockfrostProvider, BrowserWallet } from '@meshsdk/core';
import { AppWallet } from '@meshsdk/core';

import { UrlValidator } from './validators';
import { Alert } from './Alert/Alert';

import {
  SERVICE_TYPE,
  CAPTCHA_SITE_KEY,
  SEND_STATUS,
  ML_ON_CPU_REGISTRY
} from './constants';
import { imageMapper } from './containers/image';
import { constraintsMapper } from './containers/constraints';
import { Calculate_Static_NTX_GPU } from './containers/max_ntx';
import { useEffect } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { IS_PRODUCTION } from './settings';
import { LockAssetsInContract } from './tokens/LockContract';

import { resolvePlutusScriptAddress } from '@meshsdk/core';

import { scriptObject } from './tokens/utils';

import { DescriptionLinksComponent } from './components/DescriptionLinks';
import {
  validateCaptcha,
  sendDeploymentBody,
  calculateFees,
  getTransactions,
  getClaim,
  sendStatusToDMS,
  syncEndpoint,
  getFilePaths
} from './services/api';
import { ResultModal } from 'components/Modal';
import { useNotificationsContext } from 'hooks/useNotifications';
import FileTable from 'components/FileTable';
import { MNEMONIC, T_ADDRESS, T_BALANCE, T_BF_KEY } from './TestWallet';

export const AppForm = () => {
  const { wallet, connected, disconnect } = useWallet();
  const address = useAddress();
  const lovelace = useLovelace();
  const assets = useAssets();

  const [balance, setBalance] = useState(0);

  const [captcha, setCaptcha] = React.useState(false);

  const [isLoading, setIsLoading] = React.useState(false);

  const [logs, setLogs] = React.useState([]);
  const [errorLogs, setErrorLogs] = React.useState([]);

  const [modalOpen, setModalOpen] = useState(false);

  const [jobCompleted, setJobCompleted] = useState('NO');

  const [ResponseBody, setResponseBody] = useState({
    success: true,
    content: ''
  });

  const [selectedPeer, setSelectedPeer] = useState(null);

  const [containerID, setContainerID] = useState('');

  const [utxos, setUTXOs] = React.useState([]);
  const [datums, setDatums] = React.useState([]);
  const [txHashes, setTxHashes] = React.useState([]);

  const [claimResponse, setClaimResponse] = React.useState([]);

  const [showFilesTable, setShowFilesTable] = React.useState(false);
  const [filePaths, setFilePaths] = React.useState([]);
  const [selectedFile, setSelectedFile] = React.useState(null);
  useEffect(() => {
    getFilePaths().then((x) => setFilePaths(x));
  }, []);

  const [showPeers, setShowPeers] = useState(true);

  const fetchTransactions = (SizeDone, CleanTx) => {
    getTransactions(SizeDone, CleanTx)
      .then((x) => {
        if (x == null) {
          setTxHashes([]);
        } else {
          setTxHashes(x);
        }
      })
      .catch((e) => {
        setIsLoading(false);
        console.log(e);
        const error =
          e &&
          e.response &&
          e.response.data &&
          e.response.data.error &&
          e.response.data.error.length > 0
            ? e.response.data.error
            : 'Can not load data. Please try again later.';
        setErrors([error]);
        setNotifications([
          {
            msg: error,
            isPositive: false,
            createdAt: Date.now()
          },
          ...notifications.slice(0, 4)
        ]);
      });
  };

  const INTERVAL_TIME = 5 * 60 * 1000;

  useEffect(() => {
    fetchTransactions(20, '');
    const interval = window.setInterval(() => {
      fetchTransactions(20, '');
    }, INTERVAL_TIME);
  }, []);

  const sync = async () => {
    const _scriptAddress = resolvePlutusScriptAddress(scriptObject);
    syncEndpoint({ address: _scriptAddress });
    const interval = window.setInterval(() => {
      syncEndpoint({ address: _scriptAddress });
    }, INTERVAL_TIME);
  };

  const [selectedRowIndex, setSelectedRowIndex] = React.useState(null);

  const handleRowSelect = (index) => {
    setSelectedRowIndex(index);
  };

  const [transactionStatus, setTransactionStatus] = React.useState('');

  const [notifications, setNotifications] = useNotificationsContext();

  const [failingMessage, setFailingMessage] = React.useState('');

  const [socket, setSocket] = React.useState({});
  const [connectedToSocket, setConnectedToSocket] = useState(false);

  const sendConnectionToast = () => {
    toast('Cannot Connect to server, try again later!', {
      position: 'bottom-right',
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: 'dark'
    });
    setNotifications([
      {
        msg: 'Cannot Connect to server, try again later!',
        isPositive: false,
        createdAt: Date.now()
      },
      ...notifications.slice(0, 4)
    ]);
  };

  useEffect(() => {
    let s = new W3CWebSocket(SEND_STATUS);
    setSocket(s);

    s.onclose = () => {
      console.log('SOCKET Connection closed');
      setConnectedToSocket(false);
      sendConnectionToast();
    };
    s.onopen = () => {
      console.log('SOCKET Connection opened!');
      setConnectedToSocket(true);
    };
    s.onmessage = ({ data }) => {
      console.log('>>>SOCKET:', data);
      const socketData = JSON.parse(data);
      const { info, action, message, stdout, stderr } = socketData;
      if (action === 'connected') {
        console.log(message);
      } else if (action === 'log-stream-response') {
        if (!modalOpen) {
          setModalOpen(true);
          setIsJobSubmittedModal(false);
        }
        if ('stdout' in socketData) {
          setLogs((currentState) => [...currentState, stdout]);
        } else if ('stderr' in socketData) {
          setErrorLogs((currentState) => [...currentState, stderr]);
        }
      } else if (action === 'job-submitted') {
        setIsJobSubmittedModal(true);
        window.onbeforeunload = function () {
          return 'Are you sure you want to reload the page? Yo will lose all your progress';
        };
      } else if (action === 'deployment-response') {
        setIsJobSubmittedModal(false);
        console.log('DEPLOYMENT RESPONSE');
        console.log(message);
        setResponseBody(message);
        setContainerID(info);
        setModalOpen(true);
      } else if (action === 'job-completed') {
        setIsJobSubmittedModal(false);
        setJobCompleted('YES');
        setModalOpen(true);
        window.onbeforeunload = function () {};
      } else if (action === 'job-failed') {
        window.onbeforeunload = function () {};
        setJobCompleted('FAILED');
        if (message.length > 0) {
          setFailingMessage(message);
          setLogs((currentState) => [...currentState, 'ERROR: ' + message]);
        }
        if (!modalOpen) {
          setModalOpen(true);
          setIsJobSubmittedModal(false);
        }
      }
      // else if (action === 'job-completed') {
      //   setJobCompleted(true);
      // }
    };
  }, []);

  const onRecaptchaSuccess = (token) => {
    setCaptcha(token);
  };

  useEffect(() => {
    if (connected && IS_PRODUCTION) {
      wallet.getBalance().then((b) => {
        setForm({ ...form, userAddress: address });
        setBalance(+b[0].quantity);
      });
    } else {
      setBalance(0);
      setForm({ ...form, userAddress: '' });
    }
  }, [connected]);

  const [page, setPage] = React.useState(1);

  const [libs, setLibs] = React.useState([]);
  const addField = () => {
    const key = Math.floor(Math.random() * 100000000) + 1;
    setLibs([...libs, key]);
  };
  const deleteField = (index) => {
    const newLibs = [...libs];
    newLibs.splice(index, 1);
    setLibs(newLibs);
  };

  const [form, setForm] = React.useState({
    url: '',
    framework: 'Tensorflow',
    machine_type: 'gpu',
    time: 1,
    userAddress: '',
    ntx: 0,
    blockChain: 'Cardano',
    complexity: 'Low'
  });

  const [showWindow, setShowWindow] = React.useState(false);

  const [libValues, setLibValues] = React.useState({});
  const [errors, setErrors] = React.useState([]);
  const [failedopenText, setfailedopenText] = useState('');

  const [minPrice, setMinPrice] = React.useState(0);
  React.useEffect(() => {
    const p = Calculate_Static_NTX_GPU(form.complexity, form.time);
    setMinPrice(Math.ceil(p));
  }, [form]);

  const onChangingPage = () => {
    setErrors([]);

    const collectedErrors = [];

    if (form.url.trim().length === 0) {
      collectedErrors.push('Please Enter a URL');
    }
    if (!UrlValidator.test(form.url)) {
      collectedErrors.push('Please Enter a valid URL');
    }

    if (form.time.toString().trim().length === 0) {
      collectedErrors.push('Please Enter an Estimated Time');
    }

    if (form.time < 1) {
      collectedErrors.push('Estimated time must be greater than 0');
    }

    if (collectedErrors.length > 0 && IS_PRODUCTION) {
      setErrors([...collectedErrors]);
      setNotifications([
        {
          msg: collectedErrors[0],
          isPositive: false,
          createdAt: Date.now()
        },
        ...notifications.slice(0, 4)
      ]);
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    } else {
      setPage(2);
    }
  };

  const onSubmit = async () => {
    setErrors([]);

    const collectedErrors = [];

    console.log(form.ntx);
    if (form.ntx < Math.ceil(minPrice)) {
      collectedErrors.push("NTX Number Can't be less than the minimum");
    }

    if (form.ntx.toString().trim().length <= 0) {
      collectedErrors.push('Please Enter a valid NTX number');
    }

    if (!Number.isInteger(form.ntx)) {
      console.log('WIERDO: ', !Number.isInteger(form.ntx));
      collectedErrors.push('NTX Number must be an integer');
    }

    if (collectedErrors.length > 0 && IS_PRODUCTION) {
      setNotifications([
        {
          msg: collectedErrors[0],
          isPositive: false,
          createdAt: Date.now()
        },
        ...notifications.slice(0, 4)
      ]);
      setErrors([...collectedErrors]);
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    } else {
      const data = { ...form };
      const collectedLibraries = [];
      libs.forEach((lib) => {
        if (libValues[lib] !== undefined) {
          collectedLibraries.push(libValues[lib].trim());
        }
      });
      data['libs'] = collectedLibraries.filter(
        (l) => l !== undefined && l.length > 0
      );
      const keys = Object.keys(data);
      keys.forEach((key) => {
        if (typeof data[key] === 'string') {
          data[key] = data[key].trim();
        }
      });
      data['ntx'] = parseInt(data['ntx']);
      data['time'] = parseInt(data['time']);
      const postData = {
        user_type: 'SPD',
        address_user: '',
        max_ntx: data.ntx,
        blockchain: data.blockChain,
        service_type:
          data.machine_type === 'cpu' ? 'ml-training-cpu' : SERVICE_TYPE,
        params: {
          machine_type: data.machine_type,
          node_id: selectedPeer,
          resume_job: {
            resume: showFilesTable,
            progress_file: selectedFile
          },
          image_id:
            data.machine_type === 'cpu'
              ? ML_ON_CPU_REGISTRY
              : imageMapper(data.framework),
          model_url: data.url,
          packages: data.libs
        },
        constraints: { ...constraintsMapper(data.complexity), time: data.time },
        user_type: 'SPD'
      };
      console.log(postData);
      setIsLoading(true);
      const input = typeof captcha === 'string' ? captcha : '';
      validateCaptcha(input)
        .then(async ({ success }) => {
          if (success) {
            sendDeploymentBody(postData)
              .then(async (res) => {
                socket.send(
                  JSON.stringify({
                    message: {
                      transaction_status: 'success',
                      transaction_type: 'fund'
                    },
                    action: 'send-status'
                  })
                );
              })
              .catch((e) => {
                console.log();
                if (
                  e.response &&
                  e.response.data &&
                  e.response.data['deployment-error']
                ) {
                  setNotifications([
                    {
                      msg: e.response.data['deployment-error'],
                      isPositive: false,
                      createdAt: Date.now()
                    },
                    ...notifications.slice(0, 4)
                  ]);
                  // Setting error from server as a message
                  setfailedopenText(e.response.data['deployment-error']);
                  setfailedopen(true);
                } else {
                  setNotifications([
                    {
                      msg: 'Something Went Wrong, Please try again later!',
                      isPositive: false,
                      createdAt: Date.now()
                    },
                    ...notifications.slice(0, 4)
                  ]);
                }
                setfailedopen(true);
              });
          } else {
            setErrors(['Please Make sure the captcha is verified']);
            setNotifications([
              {
                msg: 'Please Make sure the captcha is verified',
                isPositive: false,
                createdAt: Date.now()
              },
              ...notifications.slice(0, 4)
            ]);
            setIsLoading(false);
          }
        })
        .catch((e) => {
          setErrors(['Something Went Wrong, Please try again later']);
          setNotifications([
            {
              msg: 'Something Went Wrong, Please try again later',
              isPositive: false,
              createdAt: Date.now()
            },
            ...notifications.slice(0, 4)
          ]);
          setIsLoading(false);
        });
    }
  };

  const [failedopen, setfailedopen] = useState(false);

  const [isJobSubmittedModal, setIsJobSubmittedModal] = useState(false);

  const end = (terminate) => {
    window.onbeforeunload = function () {};
    if (terminate) {
      socket.send(
        JSON.stringify({
          action: 'terminate-job',
          'container-id': containerID
        })
      );
    }
    setModalOpen(false);
    setForm({
      url: '',
      framework: 'Tensorflow',
      machine_type: 'gpu',
      time: 1,
      userAddress: '',
      ntx: 0,
      blockChain: 'Cardano',
      complexity: 'Low'
    });
    setLogs([]);
    setErrorLogs([]);
    setfailedopen(false);
    setCaptcha(false);
    setPage(1);
    setLibs([]);
    setMinPrice(0);
    setIsJobSubmittedModal(false);
    setLibValues({});
    setIsLoading(false);
    setJobCompleted('NO');
    setResponseBody({
      success: true,
      content: ''
    });
    setContainerID('');
    setErrors([]);
    setFailingMessage('');
    setfailedopenText('');
    setClaimResponse({});
  };

  return (
    // <NunetLoader duration={2000} onFinsih={() => {}}>
    <div className="show-fake-browser login-page">
      <ToastContainer />
      <JobSubmittedModal
        open={isJobSubmittedModal}
        closeModal={() => {
          setIsJobSubmittedModal(false);
          setIsLoading(false);
        }}
      />
      <ResultModal
        open={modalOpen}
        body={ResponseBody}
        logs={logs}
        errorLogs={errorLogs}
        jobCompleted={jobCompleted}
        failingMessage={failingMessage}
        terminate={() => end(true)}
        end={() => end(false)}
        closeModal={() => {
          setModalOpen(false);
          setIsLoading(false);
        }}
      />
      <FailedPopup
        isOpen={failedopen}
        // passing error from server to the popup
        text={failedopenText}
        handleClose={() => {
          setIsLoading(false);
          setPage(1);
          setfailedopen(false);
        }}
      />
      <Container>
        <Header>
          <Navbar appearance="inverse" style={{ background: '#0a2042' }}>
            <Navbar.Brand style={{ textAlign: 'left' }}>
              <span style={{ color: '#fff' }}>
                <img
                  src={Logo}
                  alt="logo"
                  style={{
                    width: '100px',
                    marginTop: '-5px',
                    textAlign: 'left'
                  }}
                />
              </span>
            </Navbar.Brand>
            <Nav>
              <Nav.Item
                style={{
                  transition: 'all 1s ease-in-out'
                }}
                onClick={() => {
                  if (!connectedToSocket) {
                    window.location.reload();
                  }
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '10px'
                  }}
                >
                  <div
                    className={connectedToSocket ? 'blink_green' : 'blink_me'}
                    style={{
                      background: connectedToSocket ? 'green' : 'red',
                      borderRadius: 1000,
                      padding: '.5rem',
                      width: 3,
                      height: 3,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  ></div>
                  <div>
                    {connectedToSocket ? 'Connected' : 'Failed to connect'}
                  </div>
                </div>
              </Nav.Item>
              <Nav.Menu
                icon={<NoticeIcon />}
                title={
                  notifications.length > 0 && (
                    <Badge content={notifications.length} />
                  )
                }
              >
                {notifications.map((n, i) => (
                  <Nav.Item
                    key={i}
                    style={{ borderBottom: '0.1px solid #ebebeb' }}
                  >
                    {n.isPositive ? (
                      <CheckOutline color="#4cd137" />
                    ) : (
                      <CloseOutline color="#e84118" />
                    )}
                    {n.msg}
                    <Form.HelpText
                      style={{ fontSize: '0.6rem', textAlign: 'right' }}
                    >
                      {moment(n.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
                    </Form.HelpText>
                  </Nav.Item>
                ))}
                {notifications.length > 0 && (
                  <Nav.Item
                    onClick={() => setNotifications([])}
                    style={{
                      textAlign: 'center',
                      color: '#d63031',
                      fontWeight: 'bold'
                    }}
                  >
                    Clear Notifications
                  </Nav.Item>
                )}
              </Nav.Menu>
              {/* <Button onClick={() => setShowWindow(!showWindow)}>
                Show Utxos
              </Button> */}
            </Nav>
          </Navbar>
        </Header>
        {errors.length > 0 && <Alert text={errors[0]} />}
        {page === 1 ? (
          <Content>
            <FlexboxGrid justify="center" style={{ margin: '2rem 0' }}>
              <FlexboxGrid.Item colspan={12} className="flex-panel-container">
                <Panel
                  style={{ background: 'white', padding: '10px' }}
                  className="flex-panel"
                  header={
                    <h4
                      style={{
                        textAlign: 'center',
                        fontWeight: '300',
                        marginBottom: '20px'
                      }}
                    >
                      Enter your GPU ML/Computing Requirements 🚀
                      <hr />
                    </h4>
                  }
                  shaded
                >
                  <Form fluid onSubmit={() => onChangingPage()}>
                    <Form.Group>
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        Enter your Python program link for ML or Computing:
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>Link to Python ML Model (URL)</Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>
                      <Form.Control
                        value={form.url}
                        name="url"
                        onChange={(v) =>
                          setForm({ ...form, url: v.replace(/\s/gi, '') })
                        }
                      />
                    </Form.Group>

                    <Form.Group
                      name="machine_type"
                      label="Machine type to run the ML model"
                    >
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        Choose your machine learning / computing Requirements.
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>
                              Choose your machine type CPU or GPU.
                            </Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>
                      <RadioGroup
                        name="framework"
                        inline
                        value={form.machine_type}
                        onChange={(v) => setForm({ ...form, machine_type: v })}
                      >
                        <Radio value={'gpu'}>GPU</Radio>
                        <Radio value={'cpu'}>CPU</Radio>
                      </RadioGroup>
                      {form.machine_type === 'cpu' && (
                        <DescriptionLinksComponent />
                      )}
                    </Form.Group>

                    {form.machine_type === 'gpu' && (
                      <Form.Group
                        name="framework"
                        label="Which framework does it use?"
                      >
                        <Form.ControlLabel style={{ position: 'relative' }}>
                          Available frameworks to start with:
                          <Whisper
                            trigger="hover"
                            placement="left"
                            speaker={
                              <Tooltip>
                                The framework that the ML Model uses
                              </Tooltip>
                            }
                          >
                            <IconButton
                              size="xs"
                              circle
                              style={{
                                position: 'absolute',
                                right: '0',
                                top: '-3px'
                              }}
                              icon={<InfoRoundIcon color="#78909b" />}
                            />
                          </Whisper>
                        </Form.ControlLabel>
                        <RadioGroup
                          name="framework"
                          inline
                          value={form.framework}
                          onChange={(v) => setForm({ ...form, framework: v })}
                        >
                          <Radio value={'Tensorflow'}>TensorFlow</Radio>
                          <Radio value={'Pytorch'}>PyTorch</Radio>
                          <Radio value={'Pyopencl'}>PyOpenCL</Radio>
                        </RadioGroup>
                        <DescriptionLinksComponent />
                      </Form.Group>
                    )}

                    <Form.Group name="complexity" label="Complexity">
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        Resource Usage:
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>
                              Corresponding GPU resource would be allocated
                              based on your requirement
                            </Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>
                      <RadioGroup
                        name="blockchain"
                        inline
                        value={form.complexity}
                        onChange={(v) => setForm({ ...form, complexity: v })}
                      >
                        <Radio value={'Low'}>Low</Radio>
                        <Radio value={'Moderate'}>Moderate</Radio>
                        <Radio value={'High'}>High</Radio>
                      </RadioGroup>
                    </Form.Group>

                    <Form.Group
                      style={{
                        position: 'relative',
                        background: 'rgb(245, 245, 245)',
                        padding: '10px',
                        borderRadius: '5px'
                      }}
                    >
                      <Form.ControlLabel style={{ marginBottom: '10px' }}>
                        Please enter any additional frameworks or dependencies
                        you may need:{' '}
                        <Whisper
                          trigger="hover"
                          placement="top"
                          speaker={<Tooltip>Add Dependencies</Tooltip>}
                        >
                          <IconButton
                            style={{
                              position: 'absolute',
                              right: '10px',
                              top: '4px'
                            }}
                            icon={<PlusIcon color="#45089b" />}
                            circle
                            onClick={addField}
                          ></IconButton>
                        </Whisper>
                      </Form.ControlLabel>
                      {libs.length > 0 &&
                        libs.map((l, index) => (
                          <div style={{ display: 'flex' }} key={l}>
                            <Whisper
                              trigger="hover"
                              placement="top"
                              speaker={<Tooltip>Delete</Tooltip>}
                            >
                              <IconButton
                                style={{ height: '100%', marginTop: '5px' }}
                                icon={<TrashIcon color="#ec297b" />}
                                onClick={() => deleteField(index)}
                              />
                            </Whisper>
                            <Form.Control
                              style={{ margin: '5px 0' }}
                              name={`deps${l}`}
                              value={
                                libValues[l] !== undefined ? libValues[l] : ''
                              }
                              onChange={(v) => {
                                const result = v.replace(/\s/gi, '');
                                setLibValues({ ...libValues, [l]: result });
                              }}
                              className="small_placeholder"
                              placeholder="Enter a conda package name to install with mamba, before deploying your ML Job"
                            />
                          </div>
                        ))}
                    </Form.Group>

                    <Form.Group>
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        Estimated Computation Time (in minutes)
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>
                              Expected time for your ML job to be completed
                            </Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>

                      <Form.Control
                        name="time"
                        type="number"
                        autoComplete="off"
                        value={form.time}
                        min="1"
                        onChange={(v) => setForm({ ...form, time: v })}
                      />
                    </Form.Group>

                    <Form.Group>
                      <ButtonToolbar className="right">
                        <Button
                          placement="right"
                          appearance="primary"
                          className="gr-btn"
                          size="lg"
                          type="submit"
                          disabled={!connectedToSocket}
                        >
                          Next
                          <PageNext />
                        </Button>
                      </ButtonToolbar>

                      {connectedToSocket === false && IS_PRODUCTION && (
                        <Form.HelpText style={{ textAlign: 'right' }}>
                          We can not connect to server now, please{' '}
                          <span
                            style={{ textDecoration: 'underline' }}
                            onClick={() => window.location.reload()}
                          >
                            refresh the page!
                          </span>{' '}
                          or try again later.
                        </Form.HelpText>
                      )}
                    </Form.Group>
                  </Form>
                </Panel>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Content>
        ) : page === 2 ? (
          <Content>
            <FlexboxGrid justify="center" style={{ margin: '2rem 0' }}>
              <FlexboxGrid.Item colspan={12} className="flex-panel-container">
                <Panel
                  style={{ background: 'white', padding: '10px' }}
                  className="flex-panel"
                  header={
                    <h4
                      style={{
                        textAlign: 'center',
                        fontWeight: '300',
                        marginBottom: '15px',
                        position: 'relative',
                        fontSize: '30px'
                      }}
                    >
                      <Whisper
                        trigger="hover"
                        placement="top"
                        speaker={<Tooltip>Back</Tooltip>}
                      >
                        <IconButton
                          style={{ position: 'absolute', left: '10px' }}
                          size="md"
                          onClick={() => setPage(1)}
                          icon={<ArowBack color="#45089b" />}
                        />
                      </Whisper>
                      Tokenomics
                      <hr />
                    </h4>
                  }
                  bordered
                  shaded
                >
                  <Form fluid>
                    <Form.Group>
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        User Address:
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={<Tooltip>The Address of the user</Tooltip>}
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>
                      <Input
                        as={'textarea'}
                        name="useraddress"
                        value={form.userAddress}
                        readOnly={true}
                        rows={3}
                        placeholder="Get address by connecting a wallet..."
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        Max tokens amount:
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>
                              Max tokens amount to pay for compute job
                            </Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>

                      <Form.Control
                        name="ntx"
                        type="number"
                        autoComplete="off"
                        min={0}
                        value={form.ntx}
                        onChange={(v) =>
                          setForm({ ...form, ntx: +v.replace('-', '') })
                        }
                      />
                      <Form.HelpText>
                        The Field can't be less than {Math.ceil(minPrice)} NTX
                        based on your Requirements
                      </Form.HelpText>
                    </Form.Group>
                    <Form.Group name="blockchain" label="Blockchain">
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        Blockchain:
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>
                              The blockchain needed, Ethereum is still in
                              development
                            </Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>
                      <RadioGroup
                        name="blockchain"
                        inline
                        value={form.blockChain}
                        onChange={(v) => setForm({ ...form, blockChain: v })}
                      >
                        <Radio value={'Cardano'}>Cardano</Radio>
                        <Radio disabled={true} value={'Ethereum'}>
                          Ethereum (In Development)
                        </Radio>
                      </RadioGroup>
                    </Form.Group>
                    <Divider />
                    <Form.Group>
                      <Form.ControlLabel style={{ position: 'relative' }}>
                        <Checkbox
                          onChange={() => setShowFilesTable(!showFilesTable)}
                          checked={showFilesTable}
                        >
                          Resume Job?
                        </Checkbox>
                        <Whisper
                          trigger="hover"
                          placement="left"
                          speaker={
                            <Tooltip>
                              If there is a paused job, you can resume it now
                            </Tooltip>
                          }
                        >
                          <IconButton
                            size="xs"
                            circle
                            style={{
                              position: 'absolute',
                              right: '0',
                              top: '-3px'
                            }}
                            icon={<InfoRoundIcon color="#78909b" />}
                          />
                        </Whisper>
                      </Form.ControlLabel>
                      {showFilesTable && (
                        <FileTable
                          setFile={setSelectedFile}
                          data={filePaths.map((pathObject, index) => {
                            const dir = pathObject.checkpoint_dir;
                            const file = pathObject.path;
                            const path = dir + '/' + file;
                            const date = new Date(
                              pathObject.last_modified * 1000
                            ).toISOString();
                            return { index, path, dir, file, date };
                          })}
                        />
                      )}
                    </Form.Group>

                    <Form.Group>
                      <ButtonToolbar>
                        <Button
                          appearance="primary"
                          size="lg"
                          className="gr-btn"
                          style={{ width: '100%', marginTop: '1rem' }}
                          onClick={() => setPage(3)}
                          disabled={
                            !connectedToSocket ||
                            form.ntx < Math.ceil(minPrice) ||
                            (showFilesTable && selectedFile === null)
                          }
                        >
                          {isLoading ? (
                            <Loader />
                          ) : (
                            <>
                              Next <PageNext />
                            </>
                          )}
                        </Button>
                        {connectedToSocket === false && IS_PRODUCTION && (
                          <Form.HelpText style={{ textAlign: 'right' }}>
                            We can not connect to server now, please{' '}
                            <span
                              style={{ textDecoration: 'underline' }}
                              onClick={() => window.location.reload()}
                            >
                              refresh the page!
                            </span>{' '}
                            or try again later.
                          </Form.HelpText>
                        )}
                      </ButtonToolbar>
                    </Form.Group>
                  </Form>
                </Panel>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Content>
        ) : (
          <Content>
            <FlexboxGrid justify="center" style={{ margin: '2rem 0' }}>
              <FlexboxGrid.Item colspan={12} className="flex-panel-container">
                <Panel
                  style={{ background: 'white', padding: '10px' }}
                  className="flex-panel"
                  header={
                    <h4
                      style={{
                        textAlign: 'center',
                        fontWeight: '300',
                        marginBottom: '15px',
                        position: 'relative',
                        fontSize: '30px'
                      }}
                    >
                      <Whisper
                        trigger="hover"
                        placement="top"
                        speaker={<Tooltip>Back</Tooltip>}
                      >
                        <IconButton
                          style={{ position: 'absolute', left: '10px' }}
                          size="md"
                          onClick={() => setPage(2)}
                          icon={<ArowBack color="#45089b" />}
                        />
                      </Whisper>
                      Select a peer
                      <hr />
                    </h4>
                  }
                  bordered
                  shaded
                >
                  <Form fluid>
                    <PeersPopupPage
                      setPeer={setSelectedPeer}
                      showPeers={showPeers}
                      setShowPeers={setShowPeers}
                    />
                    <Form.HelpText>
                      In case of not selecting a peer, the DMS will use the
                      recommended peer
                    </Form.HelpText>
                    <Divider />
                    {!(modalOpen || isJobSubmittedModal || isLoading) ? (
                      <ReCAPTCHA
                        sitekey={
                          process.env.REACT_APP_RUNNING_WITH_CYPRESS === 'true'
                            ? '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
                            : CAPTCHA_SITE_KEY
                        }
                        onChange={onRecaptchaSuccess}
                      />
                    ) : null}{' '}
                    <Form.Group>
                      <ButtonToolbar>
                        <Button
                          appearance="primary"
                          size="lg"
                          className="gr-btn"
                          style={{ width: '100%', marginTop: '1rem' }}
                          onClick={onSubmit}
                          disabled={
                            (connectedToSocket === false ||
                              isLoading ||
                              (showFilesTable && selectedFile === null) ||
                              (showPeers && selectedPeer === null) ||
                              !captcha) &&
                            IS_PRODUCTION
                          }
                        >
                          {isLoading ? (
                            <Loader />
                          ) : (
                            <>
                              <CheckOutline /> Submit
                            </>
                          )}
                        </Button>
                        {connectedToSocket === false && IS_PRODUCTION && (
                          <Form.HelpText style={{ textAlign: 'right' }}>
                            We can not connect to server now, please{' '}
                            <span
                              style={{ textDecoration: 'underline' }}
                              onClick={() => window.location.reload()}
                            >
                              refresh the page!
                            </span>{' '}
                            or try again later.
                          </Form.HelpText>
                        )}
                      </ButtonToolbar>
                    </Form.Group>
                  </Form>
                </Panel>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Content>
        )}
      </Container>
    </div>
    // </NunetLoader>
  );
};
