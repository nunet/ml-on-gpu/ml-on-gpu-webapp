import { Calculate_Static_NTX_GPU } from '../../containers/max_ntx';
describe('Calculate_Static_NTX_GPU Function', () => {
  test('calculates minimum estimated NTX for low resource usage', () => {
    const resourceUsage = 'Low';
    const time = 10;

    const result = Calculate_Static_NTX_GPU(resourceUsage, time);

    expect(result).toBeGreaterThan(0);
  });

  test('calculates minimum estimated NTX for moderate resource usage', () => {
    const resourceUsage = 'Moderate';
    const time = 10;

    const result = Calculate_Static_NTX_GPU(resourceUsage, time);

    expect(result).toBeGreaterThan(0);
  });

  test('calculates minimum estimated NTX for high resource usage', () => {
    const resourceUsage = 'High';
    const time = 10;

    const result = Calculate_Static_NTX_GPU(resourceUsage, time);

    expect(result).toBeGreaterThan(0);
  });
});
