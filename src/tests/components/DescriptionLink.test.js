/* eslint-disable testing-library/prefer-screen-queries */
import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { DescriptionLinksComponent } from '../../components/DescriptionLinks';
describe('DescriptionLinks Component', () => {
  it('renders all links', () => {
    const { getByText } = render(<DescriptionLinksComponent />);
    expect(getByText('TensorFlow')).toHaveAttribute(
      'href',
      'https://www.tensorflow.org/'
    );
    expect(getByText('PyTorch')).toHaveAttribute(
      'href',
      'https://pytorch.org/'
    );
  });
});
