/* eslint-disable testing-library/prefer-screen-queries */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { FailedPopup } from '../../components/FailedPopup';

describe('FailedPopup Component', () => {
  const mockHandleClose = jest.fn();

  it('renders correctly when open', () => {
    const { getByText } = render(
      <FailedPopup
        isOpen={true}
        handleClose={mockHandleClose}
        text={'Failed Message'}
      />
    );
    expect(getByText('Failed Message')).toBeInTheDocument();
  });

  it('does not render when closed', () => {
    const { queryByText } = render(
      <FailedPopup
        isOpen={false}
        handleClose={mockHandleClose}
        text={'Failed Message'}
      />
    );
    expect(queryByText('Failed Message')).not.toBeInTheDocument();
  });

  it('calls handleClose when close button is clicked', () => {
    const { getByText } = render(
      <FailedPopup
        isOpen={true}
        handleClose={mockHandleClose}
        text={'Failed Message'}
      />
    );
    fireEvent.click(getByText('Ok'));
    expect(mockHandleClose).toHaveBeenCalled();
  });
});
