import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BlocksBar } from '../../components/BlocksBar';

describe('BlocksBar Component', () => {
  test('renders the component with stdout option', () => {
    const onSelectMock = jest.fn();
    const errorCount = 0;

    render(
      <BlocksBar
        active="stdout"
        onSelect={onSelectMock}
        errorCount={errorCount}
      />
    );

    expect(screen.getByText('stdout.log')).toBeInTheDocument();
  });

  test('renders the component with stderr option when errorCount is greater than 0', () => {
    const onSelectMock = jest.fn();
    const errorCount = 1;

    render(
      <BlocksBar
        active="stdout"
        onSelect={onSelectMock}
        errorCount={errorCount}
      />
    );

    expect(screen.getByText('stderr.log')).toBeInTheDocument();
  });

  test('does not render stderr option when errorCount is 0', () => {
    const onSelectMock = jest.fn();
    const errorCount = 0;

    render(
      <BlocksBar
        active="stdout"
        onSelect={onSelectMock}
        errorCount={errorCount}
      />
    );

    expect(screen.queryByText('stderr.log')).not.toBeInTheDocument();
  });
});
