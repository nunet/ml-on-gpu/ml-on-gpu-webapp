import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { ToasterError } from '../../components/ToastError';

describe('ToasterError Component', () => {
  test('renders the component with the provided error text', () => {
    const errorText = 'This is an error message';

    render(<ToasterError text={errorText} />);

    // Check if the component renders with the provided error text
    expect(screen.getByText(errorText)).toBeInTheDocument();
  });

  test('renders the component with an error type', () => {
    const errorText = 'This is an error message';

    render(<ToasterError text={errorText} />);

    // Check if the component renders with the error type
    expect(screen.getByRole('alert')).toHaveClass('rs-message-error');
  });

  test('renders the component with a close button', () => {
    const errorText = 'This is an error message';

    render(<ToasterError text={errorText} />);

    // Check if the component renders with a close button
    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
