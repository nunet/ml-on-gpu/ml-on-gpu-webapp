import { ExtractJSONResponse } from '../../helpers/ExtractJsonResponse';

describe('ExtractJSONResponse Function', () => {
  test('returns an empty object for a non-JSON string', () => {
    const nonJSONString = 'Not a JSON string';

    const result = ExtractJSONResponse(nonJSONString);

    expect(result).toEqual({});
  });

  test('returns a JSON object for a valid JSON string', () => {
    const jsonString = '{"key": "value"}';

    const result = ExtractJSONResponse(jsonString);

    expect(result).toEqual('"{\\"key\\": \\"value\\"}"');
  });

  test('returns an empty object for an empty string', () => {
    const emptyString = '';

    const result = ExtractJSONResponse(emptyString);

    expect(result).toEqual({});
  });

  test('returns an empty object for a string starting with non-JSON character', () => {
    const stringWithNonJSONCharacter = 'Not a JSON string { "key": "value" }';

    const result = ExtractJSONResponse(stringWithNonJSONCharacter);

    expect(result).toEqual({});
  });
});
