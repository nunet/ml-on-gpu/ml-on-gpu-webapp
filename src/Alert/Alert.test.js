import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // for additional matchers
import { Alert } from './Alert'; // Adjust the path based on your project structure

describe('Alert Component', () => {
  test('renders alert with text', () => {
    const alertText = 'This is an alert message';
    render(<Alert text={alertText} />);

    // Check if the alert with the specified text is rendered
    const alertElement = screen.getByText(alertText);
    expect(alertElement).toBeInTheDocument();

    // Check if the alert has the correct class
    expect(alertElement).toHaveClass('alert_box');
  });
});
