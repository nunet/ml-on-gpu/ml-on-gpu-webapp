export const ExtractJSONResponse = (str: String) => {
  if (str === '') {
    return {};
  }
  if (str.split('')[0] != '{') {
    return {};
  } else {
    return JSON.stringify(str);
  }
};
