import { BrowserWallet } from "@meshsdk/core";

export const enableWallet = async (walletName) => {
  const wallet = await BrowserWallet.enable(walletName);
  return wallet;
};
