import { getProvisioned, onboardDeviceOnDMS } from './api';

const OnboardingRequestFactory = ({ walletAddress, cpu, memory }) => {
  return {
    payment_addr: walletAddress,
    cpu,
    memory,
    cardano: false,
    channel: 'nunet-test'
  };
};

export const prepareRequest = async (walletAddress: String) => {
  const { cpu, memory } = await getProvisioned();
  const requestParameters = OnboardingRequestFactory({
    walletAddress,
    cpu,
    memory
  });
  const response = await onboardDeviceOnDMS(requestParameters);
  return response;
};
