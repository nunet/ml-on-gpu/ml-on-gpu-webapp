import axios from 'axios';
import {
  DMS_ENDPOINT,
  TRANSACTION_STATUS_ENDPOINT,
  REQUEST_SERVICE_ENDPOINT,
  PEERS_ENDPOINT,
  DMS,
  SYNC_ENDPOINT
} from '../constants';

export const validateCaptcha = async (token: String) => {
  const res = await axios.post('https://recaptcha.nunet.io/compute-request', {
    token
  });
  return res.data;
};

export const sendStatusToDMS = async (req) => {
  const res = await axios.post(TRANSACTION_STATUS_ENDPOINT, req);
  return res.data;
};

export const syncEndpoint = async (req) => {
  const res = await axios.post(SYNC_ENDPOINT, req);
  return res.data;
};

export const sendDeploymentBody = async (postData) => {
  return await axios.post(REQUEST_SERVICE_ENDPOINT, postData, {
    timeout: 20000 // 10 seconds
  });
};

export const getProvisioned = async () => {
  const res = await axios.get(`${DMS_ENDPOINT}/provisioned`);
  return res.data;
};

export const getPeers = async () => {
  const res = await axios.get(`${PEERS_ENDPOINT}`);
  return res;
};

export const onboardDeviceOnDMS = async (req) => {
  const res = await axios.post(`${DMS_ENDPOINT}/onboard`, req);
  console.log('ONBOARDING RESPONSE', res.data);
  return res.data;
};

export const calculateFees = async (size) => {
  try {
    const res = await axios.get(
      'https://cardano-mainnet.blockfrost.io/api/v0/epochs/latest/',
      {
        headers: {
          project_id: 'mainnet54gkdL377fXM7prfXwDKevQq8ngk2bzR'
        }
      }
    );
    const { min_fee_a, min_fee_b } = res.data;
    console.log('SIZE', size);
    console.log('FEES', min_fee_a * size + min_fee_b);
    return min_fee_a * size + min_fee_b;
  } catch (error) {
    return error;
  }
};

export const getTransactions = async (SizeDone = 20, CleanTx) => {
  const res = await axios.get(`http://${DMS}/api/v1/transactions`, {
    params: {
      size_done: SizeDone,
      clean_tx: CleanTx
    }
  });
  return res.data;
};

export const getFilePaths = async () => {
  const res = await axios.get(`http://${DMS}/api/v1/run/checkpoints`);
  return res.data;
};

const ObjectFactory = (address: String, txHash: String) => {
  return {
    compute_provider_address: address,
    tx_hash: txHash,
    user_type: 'CPD'
  };
};

export const getClaim = async (address: String, txHash: String) => {
  const res = await axios.post(
    `http://${DMS}/api/v1/transactions/request-reward`,
    ObjectFactory(address, txHash)
  );
  console.log('CLAIM');
  return res.data;
};

export const blockchainProvider = async (
  scriptAddress: String,
  env: String
) => {
  const res = await axios.post(`http://preprod.cardano.nunet.io/api/v1/utxo`, {
    scriptAddress,
    env
  });
  return res.data;
};
