import { capitalizeFirstLetter } from 'helpers/Capitalize';
import { Nav } from 'rsuite';
import { TableBadge } from './TableBadge';

export const TableTabs = ({ active, onSelect, data, ...props }) => {
  const tabs = ['done', 'running', 'withdraw', 'distribute', 'refund'];
  return (
    <Nav
      {...props}
      activeKey={active}
      onSelect={onSelect}
      style={{ marginBottom: 1 }}
    >
      {tabs.map((tab, index) => (
        <Nav.Item
          eventKey={tab}
          // disabled={
          //   data.filter((d) => d.transaction_type === tab).length === 0
          // }
          key={index}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row'
            }}
          >
            <span>{capitalizeFirstLetter(tab)}</span>
            {data.filter((d) => d.transaction_type.startsWith(tab)).length >
              0 && (
              <TableBadge
                n={
                  data.filter((d) => d.transaction_type.startsWith(tab)).length
                }
              />
            )}
          </div>
        </Nav.Item>
      ))}
    </Nav>
  );
};
