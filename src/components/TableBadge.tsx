import React from 'react';
import { Badge } from 'rsuite';

export const TableBadge = ({ n }) => {
  return <Badge style={{ marginLeft: '0.5rem' }} color="blue" content={n} />;
};
