import React, { useState } from 'react';
import { Table, Button, Message, IconButton } from 'rsuite';
import { convertDateFormat } from '../tokens/utils';
import ArrowDown from '@rsuite/icons/ArrowDown';
import ArrowRight from '@rsuite/icons/ArrowRight';
import { TableTabs } from './TableTabs';
import { capitalizeFirstLetter } from '../helpers/Capitalize';

const { Column, HeaderCell, Cell } = Table;

function UtxosTable(props) {
  const [active, setActive] = useState('done');
  const [desiredIndex, setDesiredIndex] = useState<any>(null);

  const [show, setShow] = useState<boolean>(false);
  const data = props.data;

  const [sortColumn, setSortColumn] = React.useState<any>();
  const [sortType, setSortType] = React.useState<any>();
  const [loading, setLoading] = React.useState<any>(false);

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setSortColumn(sortColumn);
      setSortType(sortType);
    }, 200);
  };

  const sortData = (d) => {
    if (sortColumn && sortType) {
      return d.sort((a, b) => {
        let x = a[sortColumn];
        let y = b[sortColumn];
        if (typeof x === 'string') {
          x = x.charCodeAt(0);
        }
        if (typeof y === 'string') {
          y = y.charCodeAt(0);
        }
        if (sortType === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return d;
  };

  function truncateString(str, maxLength) {
    if (str.length > maxLength) {
      return str.substring(0, maxLength) + '...';
    }
    return str;
  }
  return (
    <div
      style={{
        background: '#f0f9ff',
        borderRadius: '10px',
        padding: '0 .5rem',
        transition: 'all ease-in-out 0.5s'
      }}
    >
      <Message
        style={{ marginBottom: '0.5rem' }}
        onClick={() => {
          setShow(!show);
          props.fetchTransactions(20, '');
        }}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          <h5>Transactions</h5>
          <IconButton
            icon={!show ? <ArrowDown /> : <ArrowRight />}
            color="blue"
            appearance="primary"
            circle
            size="xs"
          />
        </div>
      </Message>
      {show && (
        <>
          <TableTabs
            appearance="tabs"
            active={active}
            onSelect={setActive}
            data={data}
          />
          <Table
            bordered
            cellBordered
            sortColumn={sortColumn}
            sortType={sortType}
            onSortColumn={handleSortColumn}
            loading={loading}
            data={sortData(
              data
                .map((row, index) => ({
                  id: index,
                  from: truncateString(row.fields[0], 10),
                  amount: row.fields[2],
                  date: convertDateFormat(row.timestamp),
                  transaction_type: row.transaction_type,
                  tx_hash: row.tx_hash,
                  utxo: row.utxo
                }))
                .filter((row) => row.transaction_type.startsWith(active))
                .map((row, index) => ({ count: index + 1, ...row }))
            )}
          >
            <Column flexGrow={1} sortable>
              <HeaderCell>#</HeaderCell>
              <Cell dataKey="count" />
            </Column>
            <Column flexGrow={2} sortable>
              <HeaderCell>From</HeaderCell>
              <Cell dataKey="from" />
            </Column>

            <Column flexGrow={1} sortable>
              <HeaderCell>Amount</HeaderCell>
              <Cell dataKey="amount" />
            </Column>

            <Column flexGrow={2} sortable>
              <HeaderCell>Date</HeaderCell>
              <Cell dataKey="date" />
            </Column>

            <Column>
              <HeaderCell>...</HeaderCell>

              <Cell style={{ padding: '6px' }}>
                {(rowData) => (
                  <>
                    {active === 'refund' ||
                    active === 'distribute' ||
                    active === 'distribute-50' ||
                    active === 'distribute-75' ? (
                      <Button
                        size="xs"
                        appearance="primary"
                        onClick={() => {
                          props.handleSend(data[+rowData.id].tx_hash);
                          setDesiredIndex(+rowData.id as any);
                        }}
                      >
                        {capitalizeFirstLetter(active)}
                      </Button>
                    ) : (
                      <span>{capitalizeFirstLetter(active)}</span>
                    )}
                  </>
                )}
              </Cell>
            </Column>
          </Table>
          <div style={{ textAlign: 'right', padding: '1rem' }}>
            <Button
              size="sm"
              color="red"
              appearance="primary"
              disabled={
                data.filter((d) => d.transaction_type.startsWith(active))
                  .length === 0 ||
                (active !== 'done' && active !== 'withdraw')
              }
              onClick={() => props.fetchTransactions(20, active)}
            >
              Clear
            </Button>
          </div>
          {props.claimResponse && props.claimResponse.reward_type && (
            <div
              style={{
                width: '905',
                border: '1px solid grey',
                borderRadius: '5px',
                padding: '1rem',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <div style={{ fontSize: '1.2rem' }}>
                <b>{props.claimResponse.reward_type as string}</b> this
                transaction?
              </div>
              <div style={{ display: 'flex', gap: '0.75rem' }}>
                <Button
                  appearance="primary"
                  onClick={() => {
                    props.walletJob(
                      data[desiredIndex as any].utxo,
                      data[desiredIndex as any].fields[2],
                      props.claimResponse.reward_type,
                      data[desiredIndex as any].tx_hash
                    );
                  }}
                >
                  Ok
                </Button>
                <Button
                  appearance="default"
                  onClick={() => {
                    props.cancel();
                  }}
                >
                  Cancel
                </Button>
              </div>
            </div>
          )}
          {props.transactionStatus.length > 0 && (
            <div
              style={{
                width: '905',
                border: '1px solid grey',
                borderRadius: '5px',
                padding: '1rem',
                display: 'flex',
                alignItems: 'center',
                fontSize: '1.2rem',
                color: props.transactionStatus === 'success' ? 'green' : 'red'
              }}
            >
              {props.transactionStatus === 'success'
                ? 'Successful Operation'
                : 'Something went wrong!'}
            </div>
          )}
        </>
      )}
    </div>
  );
}

export default UtxosTable;
