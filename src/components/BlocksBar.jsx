import { Nav } from 'rsuite';

export const BlocksBar = ({ active, onSelect, errorCount, ...props }) => {
  return (
    <Nav
      {...props}
      activeKey={active}
      onSelect={onSelect}
      style={{ marginBottom: 50 }}
    >
      <Nav.Item eventKey="stdout">stdout.log</Nav.Item>
      {errorCount > 0 && <Nav.Item eventKey="stderr">stderr.log</Nav.Item>}
    </Nav>
  );
};
