import React, { useRef, useEffect } from 'react';
import { Modal, Button, Loader, Form } from 'rsuite';
import { SocketResponseBody } from 'types/socketResponseBody';
import ArrowRight from '@rsuite/icons/ArrowRight';
import Trash from '@rsuite/icons/Trash';
import Exit from '@rsuite/icons/CloseOutline';
import CheckOutline from '@rsuite/icons/CheckOutline';
import Reload from '@rsuite/icons/Reload';
import { CopyBlock, dracula } from 'react-code-blocks';
import { BlocksBar } from './BlocksBar';

export const ResultModal = (props: {
  open: boolean;
  body: SocketResponseBody;
  logs: string[];
  errorLogs: string[];
  terminate: () => {};
  end: () => {};
  closeModal: () => {};
  jobCompleted: string;
  failingMessage: string;
}) => {
  const bottomRef = useRef<any>(null);
  const bottomRef2 = useRef<any>(null);

  const [active, setActive] = React.useState('stdout');

  useEffect(() => {
    bottomRef.current?.scrollIntoView({ behavior: 'smooth' });
  }, [props.logs]);

  useEffect(() => {
    bottomRef2.current?.scrollIntoView({ behavior: 'smooth' });
  }, [props.errorLogs]);

  return (
    <Modal backdrop={'static'} keyboard={false} open={props.open}>
      <Modal.Body>
        {props.jobCompleted === 'NO' ? (
          <h2
            style={{
              color: props.body.success ? '#2e9ad6' : 'red',
              textAlign: 'center',
              marginBottom: '1rem',
              fontWeight: '400'
            }}
          >
            {props.body.success ? 'Job is Running' : 'Operation Failed'}
            {props.body.success ? (
              <Loader
                size="sm"
                style={{
                  marginLeft: '1rem',
                  marginTop: '-.5rem',
                  color: 'blue'
                }}
              />
            ) : (
              <Exit
                color="red"
                style={{ marginLeft: '.5rem', marginTop: '-.5rem' }}
              />
            )}
          </h2>
        ) : props.jobCompleted === 'YES' ? (
          <h2
            style={{
              color: '#2e9ad6',
              textAlign: 'center',
              marginBottom: '1rem'
            }}
          >
            Operation Is Completed. Check results!
          </h2>
        ) : (
          <h2
            style={{
              color: 'red',
              textAlign: 'center',
              marginBottom: '1rem',
              fontWeight: '400'
            }}
          >
            Operation Failed
            <Exit
              color="red"
              style={{ marginLeft: '.5rem', marginTop: '-.5rem' }}
            />
          </h2>
        )}
        {props.body.success === true ||
        props.jobCompleted === 'NO' ||
        props.jobCompleted === 'YES' ? (
          <BlocksBar
            appearance="tabs"
            active={active}
            onSelect={setActive}
            errorCount={props.errorLogs.length}
          />
        ) : null}
        {props.body.success ? (
          <>
            {active === 'stdout' ? (
              <div className="code_block">
                <CopyBlock
                  text={
                    props.logs.length <= 0 && props.jobCompleted === 'YES'
                      ? 'Failed To Load Logs'
                      : props.logs.join('\n')
                  }
                  language={'bash'}
                  showLineNumbers={true}
                  startingLineNumber={1}
                  codeBlock
                  theme={dracula}
                />
                <div
                  style={{ float: 'left', clear: 'both' }}
                  ref={bottomRef}
                ></div>
              </div>
            ) : (
              <div className="code_block">
                <CopyBlock
                  text={props.errorLogs.join('\n')}
                  language={'bash'}
                  showLineNumbers={true}
                  startingLineNumber={1}
                  codeBlock
                  theme={dracula}
                />
                <div
                  style={{ float: 'left', clear: 'both' }}
                  ref={bottomRef2}
                ></div>
              </div>
            )}
            <Form.HelpText
              style={{
                marginTop: '2rem',
                fontSize: '1rem',
                background: '#eee',
                color: '#333',
                fontWeight: '500',
                padding: '0.8rem',
                borderLeft: '7px solid rgb(0,167,157)',
                borderRadius: '5px'
              }}
            >
              As your job executes through time, you can check the job progress at regular intervals to monitor and observe its progress.
            </Form.HelpText>
            <Button
              href={props.body.content as string}
              target="_blank"
              appearance="primary"
              size="lg"
              className="gr-btn"
              disabled={props.body.content.length <= 0}
              style={{
                textAlign: 'center',
                width: '100%',
                margin: ' 1rem auto 0rem auto'
              }}
            >
              {props.body.content.length <= 0 ? (
                <>
                  <span>Failed to fetch logs</span>{' '}
                </>
              ) : (
                <>
                  Check The Job Progress
                  <ArrowRight />
                </>
              )}
            </Button>
            {props.jobCompleted === 'NO' ? (
              <Button
                appearance="primary"
                color="red"
                onClick={() => {
                  const opt = window.confirm(
                    'You will lose all your progress if you terminated the job now! are you sure you want to terminate?'
                  );
                  if (opt) {
                    props.terminate();
                  }
                }}
                size="lg"
                style={{
                  textAlign: 'center',
                  width: '100%',
                  margin: ' 1rem auto 0rem auto'
                }}
              >
                Terminate <Trash />
              </Button>
            ) : props.jobCompleted === 'YES' ? (
              <Button
                appearance="primary"
                color="green"
                onClick={props.end}
                size="lg"
                style={{
                  textAlign: 'center',
                  width: '100%',
                  margin: ' 1rem auto 0rem auto'
                }}
              >
                Finish <CheckOutline />
              </Button>
            ) : (
              <Button
                appearance="primary"
                size="lg"
                style={{
                  textAlign: 'center',
                  width: '100%',
                  margin: ' 1rem auto 0rem auto',
                  backgroundColor: '#2e9ad6',
                  transition: 'all 1s ease-in-out',
                  fontFamily: 'Roboto'
                }}
                onClick={props.terminate}
              >
                Try Again <Reload />
              </Button>
            )}
          </>
        ) : (
          <>
            {props.failingMessage && (
              <CopyBlock
                text={props.failingMessage}
                language={'bash'}
                showLineNumbers={true}
                startingLineNumber={1}
                codeBlock
                theme={dracula}
              />
            )}
            <Button
              appearance="primary"
              size="lg"
              className="gr-btn"
              style={{
                textAlign: 'center',
                width: '100%',
                margin: '3rem auto 0rem auto'
              }}
              onClick={props.closeModal}
            >
              Try Again <Reload />
            </Button>
          </>
        )}
      </Modal.Body>
    </Modal>
  );
};
