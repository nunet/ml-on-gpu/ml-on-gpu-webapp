import {
  Button,
  Whisper,
  Tooltip,
  Table,
  Form,
  Checkbox,
  IconButton
} from 'rsuite';

import InfoRoundIcon from '@rsuite/icons/InfoRound';
import { useEffect, useState } from 'react';
import { getPeers } from 'services/api';

const { Column, HeaderCell, Cell } = Table;

const columns = [
  {
    key: 'CPU',
    label: 'CPU'
  },
  {
    key: 'RAM',
    label: 'RAM'
  },
  {
    key: 'Peer ID',
    label: 'Peer ID',
    width: 500
  },
  {
    key: 'GPU',
    label: 'GPU?'
  },
  {
    key: 'GPU Type',
    label: 'GPU Type',
    width: 200
  },
  {
    key: 'GPU Total Vram',
    label: 'GPU Total Vram'
  },
  {
    key: 'GPU Free Vram',
    label: 'GPU Free Vram'
  }
];
export const PeersPopupPage = (props) => {
  const [peers, setPeers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const [peer, setPeer] = useState(null);

  useEffect(() => {
    getPeers()
      .then((data) => {
        setPeers(data.data);
        setLoading(false);
      })
      .catch(() => setError(true));
  }, []);

  return (
    <>
      <Form.Group>
        <Form.ControlLabel style={{ position: 'relative' }}>
          <Checkbox
            onChange={() => {
              props.setShowPeers(!props.showPeers);
              setPeer(null);
              props.setPeer(null);
            }}
            checked={!props.showPeers}
          >
            Choose Recommended Peer?
          </Checkbox>
          <Whisper
            trigger="hover"
            placement="left"
            speaker={
              <Tooltip>If there is a paused job, you can resume it now</Tooltip>
            }
          >
            <IconButton
              size="xs"
              circle
              style={{
                position: 'absolute',
                right: '0',
                top: '-3px'
              }}
              icon={<InfoRoundIcon color="#78909b" />}
            />
          </Whisper>
        </Form.ControlLabel>
        {props.showPeers && (
          <Table
            loading={loading}
            hover={true}
            showHeader={true}
            data={peers.map((item) => {
              return {
                CPU: item.available_resources.tot_cpu_hz,
                RAM: item.available_resources.ram,
                'Peer ID': item.peer_id,
                GPU: item.has_gpu === true ? 'Yes' : 'No',
                'GPU Type': item.gpu_info != null ? item.gpu_info[0].name : ' ',
                'GPU Total Vram':
                  item.gpu_info != null ? item.gpu_info[0].tot_vram : ' ',
                'GPU Free Vram':
                  item.gpu_info != null ? item.gpu_info[0].free_vram : ' '
              };
            })}
            autoHeight={true}
            bordered={true}
            cellBordered={true}
          >
            {columns.map((column) => {
              const { key, label, ...rest } = column;
              return (
                <Column width={150} {...rest} key={key}>
                  <HeaderCell>{label}</HeaderCell>
                  <Cell dataKey={key} />
                </Column>
              );
            })}
            <Column fixed="right">
              <HeaderCell>...</HeaderCell>
              <Cell style={{ padding: '0.5rem' }}>
                {(rowData) => {
                  return (
                    <>
                      <Button
                        style={{
                          color: rowData['Peer ID'] === peer ? 'green' : 'blue',
                          marginBottom: '.5rem'
                        }}
                        onClick={() => {
                          if (rowData['Peer ID'] === peer) {
                            setPeer(null);
                            props.setPeer(null);
                          } else {
                            setPeer(rowData['Peer ID']);
                            props.setPeer(rowData['Peer ID']);
                          }
                        }}
                      >
                        {rowData['Peer ID'] === peer ? 'Selected' : 'select'}
                      </Button>
                    </>
                  );
                }}
              </Cell>
            </Column>
          </Table>
        )}
      </Form.Group>
    </>
  );
};
