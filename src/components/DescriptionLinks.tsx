import React from 'react';
import { Form } from 'rsuite';

export const DescriptionLinksComponent = () => {
  return (
    <Form.HelpText style={{ fontSize: '12px', marginLeft: '10px' }}>
      Learn more about{' '}
      <a href="https://www.tensorflow.org/" target="_blank" rel="noreferrer">
        TensorFlow
      </a>{' '}
      ,{' '}
      <a href="https://pytorch.org/" target="_blank" rel="noreferrer">
        PyTorch
      </a>{' '}
      ,{' '}
      <a href="https://scikit-learn.org" target="_blank" rel="noreferrer">
        scikit-learn
      </a>{' '}
      and{' '}
      <a
        href="https://documen.tician.de/pyopencl/"
        target="_blank"
        rel="noreferrer"
      >
        PyOpenCL
      </a>
    </Form.HelpText>
  );
};
