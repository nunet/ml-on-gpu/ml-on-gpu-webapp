import React from 'react';
import { Modal, Button, Divider } from 'rsuite';
import { SocketResponseBody } from 'types/socketResponseBody';
import ArrowRight from '@rsuite/icons/ArrowRight';
import Trash from '@rsuite/icons/Trash';
import Check from '@rsuite/icons/CheckRound';
import Exit from '@rsuite/icons/CloseOutline';
import Reload from '@rsuite/icons/Reload';
import { CopyBlock, dracula } from 'react-code-blocks';

export const JobSubmittedModal = (props: {
  open: boolean;
  closeModal: () => {};
}) => {
  return (
    <Modal backdrop={'static'} keyboard={false} open={props.open}>
      <Modal.Body>
        <h2
          style={{
            textAlign: 'center'
          }}
          className="color-primary"
        >
          Job Is submitted
          <Check
            className="color-primary"
            style={{ marginLeft: '.5rem', marginTop: '-.5rem' }}
          />
        </h2>
        <Divider />
        <p style={{ textAlign: 'center', fontSize: '1rem' }}>
          Deployment will start shortly. Please Do Not Refresh...
        </p>
      </Modal.Body>
    </Modal>
  );
};
