import { Message } from 'rsuite';

export const ToasterError = (props) => {
  return (
    <Message showIcon type={'error'} closable>
      {props.text}
    </Message>
  );
};
