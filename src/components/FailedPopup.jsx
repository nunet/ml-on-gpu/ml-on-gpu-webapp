import {
  Modal,
  ButtonToolbar,
  Button,
  RadioGroup,
  Radio,
  Placeholder,
  Divider
} from 'rsuite';
import { CopyBlock, dracula } from 'react-code-blocks';

import InfoOutlineIcon from '@rsuite/icons/InfoOutline';

export const FailedPopup = (props) => {
  return (
    <>
      <Modal
        backdrop="static"
        keyboard={false}
        open={props.isOpen}
        onClose={props.handleClose}
      >
        <Modal.Header>
          <Modal.Title>
            <span style={{ marginLeft: '0.3rem' }}>Something Went Wrong</span>
          </Modal.Title>
          <Divider style={{ marginBottom: '0' }} />
        </Modal.Header>

        <Modal.Body style={{ marginBottom: '2rem' }}>
          <p style={{ color: '#e74c3c', fontSize: '1.2rem' }}>
            {props.text.length > 0
              ? props.text
              : 'No peers found with the matching specs.'}
          </p>
          <br />
        </Modal.Body>
        <Modal.Footer>
          <Button size="lg" onClick={props.handleClose} appearance="primary">
            Ok
          </Button>
          <Button size="lg" onClick={props.handleClose} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
