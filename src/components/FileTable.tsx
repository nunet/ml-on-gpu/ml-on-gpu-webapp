import React, { useState } from 'react';
import {
  Table,
  Button,
  Message,
  IconButton,
  Radio,
  Whisper,
  Tooltip
} from 'rsuite';
import { convertDateFormat } from '../tokens/utils';
import ArrowDown from '@rsuite/icons/ArrowDown';
import ArrowRight from '@rsuite/icons/ArrowRight';
import { TableTabs } from './TableTabs';
import { capitalizeFirstLetter } from '../helpers/Capitalize';

const { Column, HeaderCell, Cell } = Table;

function FileTable(props) {
  const [file, setFile] = useState<any>(null);
  const data = props.data;

  return (
    <Table
      data={data}
      bordered
      cellBordered
      onSortColumn={(sortColumn, sortType) => {
        console.log(sortColumn, sortType);
      }}
    >
      <Column flexGrow={2} align="center">
        <HeaderCell>Path</HeaderCell>

        <Cell>
          {(rowData) => {
            return (
              <>
                <Whisper
                  trigger="hover"
                  placement="top"
                  speaker={<Tooltip>{rowData.path}</Tooltip>}
                >
                  <span>{rowData.file}</span>
                </Whisper>
              </>
            );
          }}
        </Cell>
      </Column>

      <Column flexGrow={2}>
        <HeaderCell>Date:</HeaderCell>
        <Cell dataKey="date" />
      </Column>

      <Column>
        <HeaderCell>...</HeaderCell>
        <Cell style={{ padding: '0.5rem' }}>
          {(rowData) => {
            return (
              <>
                <Button
                  style={{
                    color: rowData.path === file ? 'green' : 'blue',
                    marginBottom: '.5rem'
                  }}
                  onClick={() => {
                    if (rowData.path === file) {
                      setFile(null);
                      props.setFile(null);
                    } else {
                      setFile(rowData.path);
                      props.setFile(rowData.path);
                    }
                  }}
                >
                  {rowData.path === file ? 'Selected' : 'select'}
                </Button>
              </>
            );
          }}
        </Cell>
      </Column>
    </Table>
  );
}

export default FileTable;
