import {
  Modal,
  ButtonToolbar,
  Button,
  RadioGroup,
  Radio,
  Placeholder,
  Divider
} from 'rsuite';
import { CopyBlock, dracula } from 'react-code-blocks';

import Exit from '@rsuite/icons/CloseOutline';
import InfoOutlineIcon from '@rsuite/icons/InfoOutline';

export const JobFailed = (props) => {
  return (
    <>
      <Modal
        backdrop="static"
        keyboard={false}
        open={props.isOpen}
        onClose={props.handleClose}
      >
        <Modal.Header>
          <Modal.Title>
            <span style={{ marginLeft: '0.3rem' }}>Something Went Wrong</span>
          </Modal.Title>
          <Divider style={{ marginBottom: '0' }} />
        </Modal.Header>

        <Modal.Body style={{ marginBottom: '2rem' }}>
          <h2
            style={{
              color: '#e74c3c',
              fontSize: '1.5rem',
              textAlign: 'center'
            }}
          >
            Job Failed{' '}
            <Exit
              color="red"
              style={{ marginLeft: '.5rem', marginTop: '-.5rem' }}
            />
          </h2>
          <br />
          <div className="code_block">
            <CopyBlock
              text={props.errorLogs.join('\n')}
              language={'bash'}
              showLineNumbers={true}
              startingLineNumber={1}
              codeBlock
              theme={dracula}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button size="lg" onClick={props.handleClose} appearance="primary">
            Ok
          </Button>
          <Button size="lg" onClick={props.handleClose} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
