import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AppProvider } from './store/provider';
import { SocketContainer } from './SocketContainer';
import { NetworkChanger } from './pages/NetworkChanger';
import { Page404 } from './pages/Page404';
import { AppForm } from './Form';
import { NotificationProvider } from 'hooks/useNotifications';

function App() {
  return (
    <AppProvider>
      <NotificationProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<AppForm />} />
            {/* <Route path="/dms" element={<NetwrokChanger />} /> */}
            <Route path="/*" element={<Page404 />} />
          </Routes>
        </BrowserRouter>
      </NotificationProvider>
    </AppProvider>
  );
}

export default App;
