import { Transaction, PlutusScript, Action, Data, Asset, UTxO } from '@meshsdk/core';

import {
  _getAssetUtxo,
  scriptObject,
  findAdaUTXOs,
  findNtxUTXO,
  polciyID
} from './utils';

export const UnLockAssetsInContract = async (
  wallet,
  address,
  computeProviderAddress,
  utxo,
  amount,
  reward_type,
  sigData,
  sigDataHash,
  sigDataDatum,
  sigAction,
  sigActionHash,
  sigActionDatum
) => {
  console.log('WALLET UNLOCK RECIEVED', utxo);
  console.log('WALLET UNLOCK RECIEVED', reward_type);

  const utxos = await findNtxUTXO(wallet);
  const lovelace = await findAdaUTXOs(wallet);

  const withdrawData: Data = {
    alternative: 0,
    fields: [
      sigData,
      sigDataHash,
      sigDataDatum.match(/\\\"(.*?)\\\"/)[1],
      sigAction,
      sigActionHash,
      sigActionDatum.match(/\\\"(.*?)\\\"/)[1]
    ]
  };

  if(reward_type == "withdraw"){
    const redeemer: Partial<Action> = {
      data: { alternative: 0, fields: [withdrawData] }
    };
  
    const tx = new Transaction({ initiator: wallet })
      .setTxInputs([lovelace])
      .setTxInputs(utxos)
      .redeemValue({
        value: utxo,
        datum: utxo,
        script: scriptObject,
        redeemer: redeemer
      })
      .setChangeAddress(address)
      .setRequiredSigners([address])
      .sendValue(address, utxo);
  
    console.log('tx:` ', tx);
  
    const unsignedTx = await tx.build();
  
    console.log('unsignedTx: ', unsignedTx);
  
    const signedTx = await wallet.signTx(unsignedTx, true);
  
    console.log('signedTx: ', signedTx);
  
    const txHash = await wallet.submitTx(signedTx);
    console.log(txHash);
  
    return txHash;
  }


  if(reward_type == "refund"){
    const redeemer: Partial<Action> = {
      data: { alternative: 1, fields: [withdrawData] }
    };
  
    const tx = new Transaction({ initiator: wallet })
      .setTxInputs([lovelace])
      .setTxInputs(utxos)
      .redeemValue({
        value: utxo,
        datum: utxo,
        script: scriptObject,
        redeemer: redeemer
      })
      .setChangeAddress(address)
      .setRequiredSigners([address])
      .sendValue(address, utxo);
  
    console.log('tx:` ', tx);
  
    const unsignedTx = await tx.build();
  
    console.log('unsignedTx: ', unsignedTx);
  
    const signedTx = await wallet.signTx(unsignedTx, true);
  
    console.log('signedTx: ', signedTx);
  
    const txHash = await wallet.submitTx(signedTx);
    console.log(txHash);
  
    return txHash;
  }

  if(reward_type == "distribute-50"){
    const redeemer: Partial<Action> = {
      data: { alternative: 2, fields: [withdrawData] }
    };


    let ntxQuantityForComputeProvider = Math.floor(amount * .50)
    let ntxQuantityForUser = (amount%2 == 1 ? ( ntxQuantityForComputeProvider +1 ) : ntxQuantityForComputeProvider )


    let ntxForUser: Asset = {
      unit: polciyID,
      quantity: '' + ntxQuantityForUser
    };
  
    let ntxForComputeProvider: Asset = {
      unit: polciyID,
      quantity: '' + ntxQuantityForComputeProvider
    }
  
  
    let lovelaceForUser: Asset = {
      unit: "lovelace",
      quantity: '2357570'
    }
  
  
  
    const userUTxO: Partial<UTxO> = {
      output: {
        address: address,
        amount: [ ntxForUser,lovelaceForUser],
      },
    };
  
    const tx = new Transaction({ initiator: wallet })
      .setTxInputs([lovelace])
      .setTxInputs(utxos)
      .redeemValue({
        value: utxo,
        datum: utxo,
        script: scriptObject,
        redeemer: redeemer
      })
      .setChangeAddress(address)
      .sendValue(address,userUTxO)
      .sendAssets(computeProviderAddress, [ntxForComputeProvider])
      .setRequiredSigners([address])
  
    console.log('tx:` ', tx);
  
    const unsignedTx = await tx.build();
  
    console.log('unsignedTx: ', unsignedTx);
  
    const signedTx = await wallet.signTx(unsignedTx, true);
  
    console.log('signedTx: ', signedTx);
  
    const txHash = await wallet.submitTx(signedTx);
    console.log(txHash);
  
    return txHash;
  }


  if(reward_type ==  "distribute-75"){
    const redeemer: Partial<Action> = {
      data: { alternative: 2, fields: [withdrawData] }
    };

    let ntxQuantityForComputeProvider = Math.floor(amount * .75)
    let ntxQuantityForUser = (amount%2 == 1 ? ( Math.floor(amount * .25) +1 ) : Math.floor(amount * .25) )



    let ntxForUser: Asset = {
      unit: polciyID,
      quantity: '' + ntxQuantityForUser
    };
  
    let ntxForComputeProvider: Asset = {
      unit: polciyID,
      quantity: '' + ntxQuantityForComputeProvider
    }
  
  
    let lovelaceForUser: Asset = {
      unit: "lovelace",
      quantity: '2357570'
    }
  
  
  
    const userUTxO: Partial<UTxO> = {
      output: {
        address: address,
        amount: [ ntxForUser,lovelaceForUser],
      },
    };
  
    const tx = new Transaction({ initiator: wallet })
      .setTxInputs([lovelace])
      .setTxInputs(utxos)
      .redeemValue({
        value: utxo,
        datum: utxo,
        script: scriptObject,
        redeemer: redeemer
      })
      .setChangeAddress(address)
      .sendValue(address,userUTxO)
      .sendAssets(computeProviderAddress, [ntxForComputeProvider])
      .setRequiredSigners([address])
  
    console.log('tx:` ', tx);
  
    const unsignedTx = await tx.build();
  
    console.log('unsignedTx: ', unsignedTx);
  
    const signedTx = await wallet.signTx(unsignedTx, true);
  
    console.log('signedTx: ', signedTx);
  
    const txHash = await wallet.submitTx(signedTx);
    console.log(txHash);
  
    return txHash;
  }

  
};
