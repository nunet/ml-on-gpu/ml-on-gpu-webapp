import {
  Transaction,
  Data,
  resolvePaymentKeyHash,
  resolvePlutusScriptAddress,
  PlutusScript,
  Asset
} from '@meshsdk/core';

import { findAdaUTXOs, findNtxUTXO, scriptObject } from './utils';

export const LockAssetsInContract = async (
  wallet,
  address,
  ntx,
  compute_provider_addr,
  estimated_price,
  metaDataHash,
  withdrawHash,
  refundHash,
  distribute75Hash,
  distribute50Hash
) => {
  let asset: Asset = {
    unit: '8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e563136d4e5458',
    quantity: '' + ntx
  };

  let assets: Asset[] = [asset];

  const scriptAddress = resolvePlutusScriptAddress(scriptObject);

  const utxos = await findNtxUTXO(wallet);
  const lovelace = await findAdaUTXOs(wallet);

  console.log(utxos, lovelace);

  const d: Data = {
    alternative: 0,
    fields: [
      resolvePaymentKeyHash(address),
      resolvePaymentKeyHash(compute_provider_addr),
      parseInt(ntx),
      metaDataHash,
      withdrawHash,
      refundHash,
      distribute75Hash,
      distribute50Hash
    ]
  };
  console.log(d);

  const tx = new Transaction({ initiator: wallet })
    .setTxInputs([lovelace])
    .setTxInputs(utxos)
    .setChangeAddress(address)
    .setRequiredSigners([address])
    .sendAssets(
      {
        address: scriptAddress,
        datum: {
          value: d,
          inline: true
        }
      },
      assets
    );

  console.log('tx: ', tx);

  const unsignedTx = await tx.build();

  console.log('unsignedTx: ', unsignedTx);

  const signedTx = await wallet.signTx(unsignedTx);

  console.log('signedTx: ', signedTx);

  const txHash = await wallet.submitTx(signedTx);
  console.log(txHash);
  return [txHash, tx.size];
};
