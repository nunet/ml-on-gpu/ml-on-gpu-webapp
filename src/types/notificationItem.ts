export interface NotificationItem {
  msg: string;
  isPositive: boolean;
  createdAt: Date;
}
