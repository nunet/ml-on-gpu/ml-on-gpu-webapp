export interface SocketResponseBody {
  success: Boolean;
  content: String;
}
