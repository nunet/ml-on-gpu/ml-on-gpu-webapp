export const MNEMONIC = [
  'list',
  'ride',
  'belt',
  'trouble',
  'virtual',
  'hero',
  'elder',
  'online',
  'assume',
  'buffalo',
  'erode',
  'cook'
];

export const T_ADDRESS =
  'addr_test1qr2ksdr6eflas96uufcxw0pvnrxp9dwpap79kkjw66r4msttxhmgqc88jtzz5sd6wj6shjmtana4h2twh8v39tktpjgqmx93wq';

export const T_BALANCE = {
  unit: 'lovelace',
  quantity: '1000000000'
};

export const T_BF_KEY = 'mainnet54gkdL377fXM7prfXwDKevQq8ngk2bzR';
