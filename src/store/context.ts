import React from 'react';
import { state } from './state';

export const AppContext = React.createContext(state);
