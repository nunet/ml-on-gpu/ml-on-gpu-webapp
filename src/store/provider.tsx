import React from "react";
import { AppContext } from "./context";
import { state } from "./state";

export const AppProvider = (props) => {
  return (
    <AppContext.Provider value={state}>{props.children}</AppContext.Provider>
  );
};
