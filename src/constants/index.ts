export const TENSORFLOW_REGISTRY =
  'registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/tensorflow';

export const PYTORCH_REGISTRY =
  'registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/pytorch';

export const PYOPENCL_REGISTRY =
  'registry.gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/develop/pyopencl';

export const ML_ON_CPU_REGISTRY =
  'registry.gitlab.com/nunet/ml-on-gpu/ml-on-cpu-service/develop/ml-on-cpu';

export const SERVICE_TYPE = 'ml-training-gpu';

export const SCRIPT_ADDRESS =
  'addr_test1wplx9dwzmn986k48kwmqn75yjlhlwcy094euq8c7s2ws8xc5k5uu6';

export const DMS ='localhost:9999';

// export const BACKEND_ENDPOINT = `ws://${DMS}/api/v1/run/deploy`;
export const REQUEST_SERVICE_ENDPOINT = `http://${DMS}/api/v1/run/request-service`;
export const DMS_ENDPOINT = `http://${DMS}/api/v1/onboarding`;
export const SEND_STATUS = `ws://${DMS}/api/v1/run/deploy`;
export const PEERS_ENDPOINT = `http://${DMS}/api/v1/peers/dht/dump`;

export const TRANSACTION_STATUS_ENDPOINT = `http://${DMS}/api/v1/run/send-status`;

export const SYNC_ENDPOINT = `http://${DMS}/api/v1/transactions/update-status`;

export const CAPTCHA_SITE_KEY = '6Lea9LslAAAAAB2_xqO1f09rgVBIq_KrjGQFe9kE';
