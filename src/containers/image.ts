import {
  PYTORCH_REGISTRY,
  TENSORFLOW_REGISTRY,
  PYOPENCL_REGISTRY
} from '../constants';

const dict = {
  Tensorflow: TENSORFLOW_REGISTRY,
  Pytorch: PYTORCH_REGISTRY,
  Pyopencl: PYOPENCL_REGISTRY
};

export const imageMapper = (t) => dict[t];
