#!/bin/bash

if ! command -v jq &>/dev/null ; then  apt update && apt install -y jq; fi

PROJECT_ROOT=$(pwd)
OUTPUT_DIR=$PROJECT_ROOT/dist
NSPD_VERSION=$(cat package.json | jq -r .version)
BUILD_DIR=$OUTPUT_DIR/service-provider-dashboard-v$NSPD_VERSION

mkdir -p $OUTPUT_DIR

npm install --legacy-peer-deps
# npm i @meshsdk/core
npm run build
cp -Prf $PROJECT_ROOT/maint-scripts/service-provider-dashboard $BUILD_DIR
cp -Prf build/* $BUILD_DIR/var/www/service-provider-dashboard/
rm $BUILD_DIR/var/www/service-provider-dashboard/.gitkeep
chmod 0755 -R $BUILD_DIR


NSPD_INST_SIZE=$(du -sk $BUILD_DIR | awk '{ print $1 }')   
sed -i "s/Version:.*/Version: $NSPD_VERSION/g" $BUILD_DIR/DEBIAN/control
sed -i "s/Installed-Size:.*/Installed-Size: $NSPD_INST_SIZE/g" $BUILD_DIR/DEBIAN/control

dpkg-deb --build $BUILD_DIR $OUTPUT_DIR

if [[ -v GITLAB_CI ]] ; then
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $OUTPUT_DIR/service-provider-dashboard_${NSPD_VERSION}_amd64.deb ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/ml-on-gpu-webapp/${NSPD_VERSION}/ml-on-gpu-webapp_${NSPD_VERSION}_amd64.deb
    cp $OUTPUT_DIR/service-provider-dashboard_${NSPD_VERSION}_amd64.deb $OUTPUT_DIR/service-provider-dashboard_latest_amd64.deb
    curl -X POST -H "Content-Type: application/json" -H "$HOOK_TOKEN_HEADER_NAME: $HOOK_TOKEN_HEADER_VALUE" -d "{\"project\" : \"SPD\", \"version\" : \"${NSPD_VERSION}\", \"commit\" : \"$CI_COMMIT_SHORT_SHA\", \"commit_msg\" : \"$CI_COMMIT_MESSAGE\", \"package_url\" : \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/ml-on-gpu-webapp/${NSPD_VERSION}/ml-on-gpu-webapp_${NSPD_VERSION}_amd64.deb\"}" $NUNETBOT_DMS_BUILD_ENDPOINT
fi
