# Nunet Webapp

Nunet Service Provider webapp

## Development

To set up the development environment, you need to have [Node.js](https://nodejs.org/en/download/) installed on your device. Then, navigate to the base directory of the project and run the following commands in your terminal:

```bash
npm install
```

```bash
npm start
```

## Build

To build the project, you need to have [Node.js](https://nodejs.org/en/download/) installed on your device. Then, navigate to the base directory of the project and run the following commands in your terminal:

```bash
npm install
```

```bash
npm run build
```

## Usage

After building the project, copy the `server.py` and `tests.py` files located in the project base directory into the newly generated `build` directory. Then, open a terminal in the `build` directory and run the `server.py` file.

## Downloading and Installing Typhoon Wallet Extension

1. Go to the Typhoon wallet website at [https://typhoon.network/](https://typhoon.network/).
2. Click on the "Download" button on the top menu bar.
3. Select your preferred operating system and download the Typhoon wallet extension.
4. Install the extension by following the prompts in your web browser.
5. Once installed, the Typhoon wallet extension should appear in your web browser's extension menu.

## Using Typhoon Wallet Extension in the Web App Extension

1. Open your web app in your web browser.
2. Click on the Typhoon wallet extension icon in your browser's extension menu.
3. Create a new Typhoon wallet or import an existing one if you have one.
4. Once your wallet is set up, you can use it to make cryptocurrency transactions within your web app.

That's it! With the Typhoon wallet extension installed and set up, you can easily make cryptocurrency transactions within your web app.


## The Deb(Linux) Package
To run the deb package, download it on your Linux device and run the following command:
```bash
sudo dpkg -i <filename>
```

# Compute Request Web App Docs

The Compute Request web app allows you to deploy your machine learning models on our machines with ease. Simply define your requirements and submit your request.

![Sequence Diagram](./sequence.png)

## Supported Wallets

We currently support the Typhoon and Eternl wallets for payment of your machine slot.

## Steps to Deploy Your Machine Learning Model

Follow these steps to deploy your machine learning model on our machines:

- Connect your wallet.
- Provide a link to your machine learning model.
- Choose your machine type, either CPU or GPU.
- Choose your framework. By default, CPU uses scikit-learn.
- Define resource usage.
- Define your model dependencies (e.g. numpy or matplotlib).
- Define the estimated computation time.
- Define the maximum tokens amount to pay for the compute job.
- Choose your blockchain. Currently only Cardano is available; Ethereum is still in development.
- Submit your request.
- Pay for your compute request with your wallet window that will open on submitting.
- Wait for your model to be deployed. The web app will notify you when it's done.

We hope you find our Compute Request web app helpful. If you have any questions or concerns, please don't hesitate to reach out to our support team.

# License

[Nunet](https://www.nunet.io/)


