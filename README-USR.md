# What is the Service Provider Dashboard?

The Service Provider Dashboard (SPD) is an intuitive and accessible platform crafted specifically for a diverse range of users, including end-users, machine learning developers, and researchers. Its primary purpose is to streamline the process of requesting CPU/GPU machine learning jobs on compute provider machines, enabling users to focus on their projects without worrying about infrastructure complexities.

By harnessing the power of NTX tokens, users can seamlessly access cutting-edge, decentralized cloud-based computational resources for executing their machine learning projects. This innovative approach not only simplifies the process but also democratizes access to advanced computing capabilities.

The SPD is compatible with widely-used machine learning libraries, such as TensorFlow, PyTorch, and scikit-learn, ensuring that users can effortlessly integrate their preferred tools and frameworks. Moreover, the platform provides the flexibility to run jobs on either CPUs or GPUs, catering to various computational needs and budget constraints.

Designed with a user-centric approach, the SPD's simple interface allows users to easily submit their ML models, define resource usage based on their job requirements, and keep track of their job's progress in real-time. This level of transparency and control empowers users to manage their machine learning jobs effectively and efficiently, ultimately facilitating and accelerating the development & deployment of innovative AI solutions.

**Note**: If you are a developer, please check out [these instructions](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-webapp/-/blob/develop/README.md).

# How to Install the Service Provider Dashboard?

## Prerequisites:

Before you install the NuNet ML SPD, make sure:

- you have already installed the device management service
- you are using a desktop machine (servers are currently not supported)

## Installation

To install the NuNet ML SPD on your system, follow these steps:

Step 1: Download the nunet-spd-latest.deb package using the wget command. Open a terminal window and enter the following command:

```
wget https://d.nunet.io/nunet-ml-service-provider-dashboard-latest.deb -O nunet-ml-service-provider-dashboard-latest.deb
```

This command will download the nunet-spd-latest.deb package from the provided URL.

Step 2: Install the nunet-spd-latest.deb package. After downloading the package, enter the following command in the terminal:

```
sudo apt update && sudo apt install ./nunet-ml-service-provider-dashboard-latest.deb -y
```

This command will update your system's package index and then install the nunet-spd-latest.deb package. The `-y` flag automatically accepts the installation prompts.

Step 3: Access the ML SPD. Open your preferred internet browser and visit:

```
localhost:9991
```

This URL will direct you to the SPD hosted on your local machine, where you can start using the service to manage your machine learning jobs and connect your NTX wallet.

Keep in mind that these installation instructions assume you are using a Debian-based Linux distribution, such as Ubuntu. The installation process may differ for other operating systems.



# How to Use the Service Provider Dashboard?

To use the Device Management Service (DMS) with Cardano, make sure your service provider machine connects with at least two DHT peers on NuNet. You can check this by using the `nunet peer list` command after you've set up your machine for a while.

Once confirmed, you can ask to run an ML job. This involves connecting your NTX wallet and integrating it with the Service Provider Dashboard (SPD) through the following steps:

**Step 1**: Connect your wallet to the SPD's dashboard by clicking the "Connect Wallet" button at the top right corner of your browser. This action will initiate a secure connection between your wallet and the dashboard, allowing for seamless token transactions.

**Step 2**: Access the SPD on the service provider machine and navigate to the first page of the UI at `localhost:9991`. on your preferred browser.

**Step 3**: Provide the ML Model URL by pasting the link to your Python program that contains the machine learning code.

Some examples that you can use for testing it out:

- CPU - https://gitlab.com/nunet/ml-on-gpu/ml-on-cpu-service/-/raw/develop/examples/cpu-ml-test-scikit-learn.py
- GPU - https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/raw/develop/examples/pytorch/cifar-10_checkpointed.py


**Step 4**: Choose the type of compute resource you want to use for the job:

- Select the CPU radio button if you want to run the job on a CPU-only machine. Currently supported libraries for CPU jobs are TensorFlow, PyTorch, and scikit-learn.
- Select the GPU radio button if you want to run the job on a GPU machine. Henceforth, choose either the TensorFlow or PyTorch radio button to specify the library used in the program. For GPU jobs, only TensorFlow and PyTorch are currently supported.

**Step 5**: Specify the resource usage type: Low, Moderate, or High - depending upon the complexity of your job.

**Step 6**: If your job has any dependencies, enter their names by choosing the "+" symbol. If you are using our GPU based example code mentioned in step 3, note that you would need to specify a dependency named `matplotlib`. The CPU example does not require any dependencies.

**Step 7**: Enter the expected time for your job's completion and click 'Next' to proceed to the second page of the SPD's UI.

**Step 8**: When you connect your wallet, the NTX wallet address field would be auto-filled, based on the Cardano blockchain. Your wallet address is a unique identifier that enables you to send and receive NTX tokens within the Cardano network.

**Step 9**: Select the Cardano blockchain by clicking on the corresponding radio button. This ensures that the device management service communicates with the appropriate blockchain network to facilitate the exchange of NTX tokens for machine learning job requests.

Finally, click on "Submit" to confirm your wallet connection and complete the integration process. Once connected, your NTX tokens would be used to deploy the requested job by allocating it to a compute provider machine that matches the resource requirements. You can also monitor the progress of the machine learning job through the same dashboard.

**Note**: Currently, the WebSocket on the SPD does not have session management. This affects users because when they reload the page after making a deployment, they will not receive any response. This would be improvised in the near future. But it's recommended not to reload the page after deployment and wait for some time. For further troubleshooting if you do not receive any response after waiting for some time following a deployment, you can try the following:

1. Check the network connection: The first step is to ensure that the network connection is stable and working correctly. The user can try opening a different website to confirm that the issue is not with their internet connection.
2. Clear the browser cache: Sometimes, the browser cache can cause issues when loading web pages. Clearing the cache can help resolve this problem. The user can try clearing their browser cache and then reloading the page.
3. File a bug report: If the issue persists, the user should file a bug report about the problem. They can provide details about the issue and any error messages received, which will help in diagnosing and resolving the problem.

In general, it's always a good idea to document the steps taken and any error messages received when encountering issues with a web application. This information can be helpful when seeking support or troubleshooting the problem later on.

# Understanding Issues on the Service Provider Dashboard

If you experience issues while using the dashboard, you can open the inspect console in your browser to
get more information about the error. Here's how to do it:

1. Open the dashboard in your web browser.
2. Right-click anywhere on the page and select "Inspect" from the context menu. Alternatively, you can use
the keyboard shortcut `Ctrl + Shift + I` on Windows/Linux or `Cmd + Option + I` on Mac to
open the inspect console.
3. The inspect console will open in a separate window or panel. Look for the "Console" tab, which should
be near the top of the panel.
4. If there are any errors, they will be displayed in the console with a red message.

When you hit the `/run/request-service` endpoint, you may encounter the following errors on the
browser console:

400 Bad Request: This error occurs when the JSON payload received from the webapp cannot be
parsed. This is unlikely to happen due to user error.

500 Internal Server Error: This error occurs when the DMS (Device Management Service) cannot find
the libp2p public key. This is probably because the compute provider has not been onboarded properly.

400 Bad Request: This error occurs when the estimated price received from the webapp is more than
expected. This is very unlikely to happen because the webapp guards against it.

404 Not Found: This error occurs when the DHT (Distributed Hash Table) does not have any peers
with matching specs. After analyzing and filtering machines based on the constraints section on the
payload, the DMS found no matched machine.

503 Service Unavailable: This error occurs when the DMS cannot connect to the Oracle for whatever
reason.

500 Internal Server Error: This error occurs when a new service cannot run because the DMS is
already running a service. Only one service is supported at the moment.

500 Internal Server Error: This error occurs when the DMS was not able to access the database.

200 OK: This response indicates success. It includes the `compute_provider_addr` , `estimated_price` , `signature` , and `oracle_message` .

[NuNet](https://www.nunet.io/)

# License

Service Provider Dashboard (SPD) is licensed under [The MIT License](LICENSE).

