#!/usr/bin/env python3

import os
import sys
import socket
import threading
import webbrowser
import time
from http.server import HTTPServer, CGIHTTPRequestHandler

server_object: HTTPServer = None  # type hint the server object

def start_server(ip_address: str, port: int) -> None:
    """Starts the HTTP server and serves forever.

    Args:
        ip_address (str): The IP address of the host.
        port (int): The port number to listen on.
    """
    global server_object
    server_object = HTTPServer(server_address=(ip_address, port), RequestHandlerClass=CGIHTTPRequestHandler)
    print(f"Running on http://{ip_address}:{port}")
    server_object.serve_forever()

if __name__ == '__main__':
    default_port: int = 8080
    # Check if the user has provided the port number
    if len(sys.argv) == 2:
        port: str = sys.argv[1]
        # Convert the port number to an integer
        try:
            port = int(port)
        except ValueError:
            print("Please provide a valid port number.")
            sys.exit(1)
    else:
        print(f"No port provided, running on default port {default_port}. If you want to change ports, please run with port number as an argument.")
        port: int = default_port

    hostname: str = socket.gethostname()
    ip_address: str = socket.gethostbyname(hostname)
    os.chdir('.')  # set current working directory to the script's directory

    # Start the server in a separate thread
    server_thread: threading.Thread = threading.Thread(target=start_server, args=(ip_address, port))
    server_thread.start()

    # Open the default web browser to the URL of the server
    webbrowser.open_new(f"http://{ip_address}:{port}")

    # Keep the script running until it is interrupted
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Stopping server...")
        server_object.shutdown()  # stop the server when interrupted
